package com.swift.yygh.gateway.filter;

import com.google.gson.JsonObject;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.List;
//@Component
public class GlobalAuthenticationFilter implements GlobalFilter, Ordered {

    private AntPathMatcher antPathMatcher=new AntPathMatcher();
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String path = request.getURI().getPath();
        if(antPathMatcher.match("/admin/user/**",path)){
            return chain.filter(exchange);//直接放行
        }else if(antPathMatcher.match("/admin/**",path)){
            HttpHeaders headers = request.getHeaders();
            List<String> strings = headers.get("X-Token");
            if(strings != null){
                if(strings.get(0).equals("admin-token")){
                    return chain.filter(exchange);//直接放行
                }
            }
            //重定向到登录页面
            response.setStatusCode(HttpStatus.SEE_OTHER);//设置响应码
            response.getHeaders().set(HttpHeaders.LOCATION,"http://localhost:9528");
            return response.setComplete();//结束请求
        }else{
          return out(response);
        }
    }
    private Mono<Void> out(ServerHttpResponse response) {
        //google
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("code","20001");
        jsonObject.addProperty("message","路径有误");
        jsonObject.addProperty("success","false");
        byte[] bits =jsonObject.toString().getBytes(StandardCharsets.UTF_8);

        DataBuffer buffer = response.bufferFactory().wrap(bits);
        response.setStatusCode(HttpStatus.NOT_FOUND);
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }


    @Override
    public int getOrder() {
        return 0;
    }
}
