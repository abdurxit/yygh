package com.swift.yygh.oss.controller;


import com.swift.yygh.common.result.R;
import com.swift.yygh.oss.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/user/oss")
public class OssController {
    @Autowired
    private OssService ossService;

    @PostMapping("/upload")
    public R uploadFile(MultipartFile file){
       String url= ossService.uploadFile(file);
       return R.ok().data("url",url);
    }



}
