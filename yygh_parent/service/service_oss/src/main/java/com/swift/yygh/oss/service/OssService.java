package com.swift.yygh.oss.service;

import org.springframework.web.multipart.MultipartFile;

/*-------------------------------------------------
      时 间:   2023-01-12
      讲 师:   刘  辉
      描 述:   尚硅谷教学团队
---------------------------------------------------*/
public interface OssService {
    String uploadFile(MultipartFile file);
}
