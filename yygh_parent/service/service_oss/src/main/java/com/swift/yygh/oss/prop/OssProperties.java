package com.swift.yygh.oss.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@ConfigurationProperties(prefix = "oss")
@Component
@Data
@PropertySource(value = "classpath:oss.properties")
// 1.不支持yml文件 //2.不能和@EnableConfigurationProperties一起使用
public class OssProperties {

    private String endpoint;
    private String bucket;
    private String keyid;
    private String keysecret;


}
