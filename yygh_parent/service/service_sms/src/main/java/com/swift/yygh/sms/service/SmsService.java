package com.swift.yygh.sms.service;

import com.swift.yygh.vo.msm.MsmVo;

public interface SmsService {
    boolean sendCode(String phone);

    void consume(MsmVo msmVo);
}
