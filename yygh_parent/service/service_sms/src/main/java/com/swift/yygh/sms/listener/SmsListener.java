package com.swift.yygh.sms.listener;


import com.rabbitmq.client.Channel;
import com.swift.yygh.mq.constant.MqConst;
import com.swift.yygh.sms.service.SmsService;
import com.swift.yygh.vo.msm.MsmVo;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SmsListener {


    @Autowired
    private SmsService smsService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    exchange = @Exchange(MqConst.EXCHANGE_SMS),
                    value = @Queue(MqConst.QUEUE_SMS),
                    key = MqConst.ROUTING_KEY_SMS
            )
    })
    public void consume(MsmVo msmVo, Message message, Channel channel){

        smsService.consume(msmVo);
    }

}
