package com.swift.yygh.order.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.swift.yygh.common.result.R;
import com.swift.yygh.common.utils.JwtHelper;
import com.swift.yygh.enums.OrderStatusEnum;
import com.swift.yygh.model.order.OrderInfo;
import com.swift.yygh.order.service.OrderInfoService;
import com.swift.yygh.vo.order.OrderCountQueryVo;
import com.swift.yygh.vo.order.OrderQueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author swift
 * @since 2023-01-29
 */
@RestController
@RequestMapping("/user/order")
public class OrderInfoController {


    @Autowired
    private OrderInfoService orderInfoService;

    @PostMapping("/submitOrder/{patientId}/{scheduleId}")
    public R submitOrder(@PathVariable Long patientId, @PathVariable String scheduleId){
        Long orderId=orderInfoService.submitOrder(patientId,scheduleId);
        return R.ok().data("orderId",orderId);
    }

    @PostMapping("/{pageNum}/{pageSize}")
    public R page(@PathVariable Integer pageNum,
                  @PathVariable Integer pageSize,
                  @RequestBody OrderQueryVo orderQueryVo,
                  @RequestHeader String token){

        Long userId = JwtHelper.getUserId(token);
        orderQueryVo.setUserId(userId);
        Page<OrderInfo> orderInfoPage= orderInfoService.selectPage(pageNum,pageSize,orderQueryVo);

        return R.ok().data("total",orderInfoPage.getTotal()).data("list",orderInfoPage.getRecords());
    }


    @GetMapping("/status/list")
    public R getOrderStatusList(){
        List<Map<String, Object>> statusList = OrderStatusEnum.getStatusList();

        return R.ok().data("list",statusList);
    }

    @GetMapping("/{orderId}")
    public R getOrderByOrderId(@PathVariable Long orderId){
        OrderInfo orderInfo = orderInfoService.getOrderByOrderId(orderId);
        return R.ok().data("orderInfo",orderInfo);
    }

    @PostMapping("/cacel/{orderId}")
    public R cancelOrder(@PathVariable Long orderId){
        orderInfoService.cancelOrder(orderId);
        return R.ok();
    }

    @PostMapping("/data")
    public Map<String,Object> getStatisticsData(@RequestBody OrderCountQueryVo orderCountQueryVo){
        return orderInfoService.getStatisticsData(orderCountQueryVo);
    }
}

