package com.swift.yygh.order.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
import com.swift.yygh.enums.OrderStatusEnum;
import com.swift.yygh.enums.PaymentStatusEnum;
import com.swift.yygh.enums.RefundStatusEnum;
import com.swift.yygh.model.order.OrderInfo;
import com.swift.yygh.model.order.PaymentInfo;
import com.swift.yygh.model.order.RefundInfo;
import com.swift.yygh.order.service.OrderInfoService;
import com.swift.yygh.order.service.PaymentInfoService;
import com.swift.yygh.order.service.RefundInfoService;
import com.swift.yygh.order.service.WeiPayService;
import com.swift.yygh.order.utils.ConstantPropertiesUtils;
import com.swift.yygh.order.utils.HttpClient;
import com.swift.yygh.order.utils.HttpRequestHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Service
public class WeiPayServiceImpl implements WeiPayService {

    @Autowired
    private PaymentInfoService paymentInfoService;
    @Autowired
    private OrderInfoService orderInfoService;

    @Autowired
    private RefundInfoService refundInfoService;

    @Override
    public String createNative(Long orderId) throws Exception {
        OrderInfo orderInfo = orderInfoService.getById(orderId);
        paymentInfoService.savePaymentInfo(orderInfo);

        HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");

        Map<String,String> paramMap=new HashMap<String,String>();
        paramMap.put("appid", ConstantPropertiesUtils.APPID);
        paramMap.put("mch_id",ConstantPropertiesUtils.PARTNER);
        paramMap.put("nonce_str",WXPayUtil.generateNonceStr());

        paramMap.put("body","您的医院挂号成功，及时前往医院就诊");

        paramMap.put("out_trade_no",orderInfo.getOutTradeNo());
        paramMap.put("total_fee","1");
        paramMap.put("spbill_create_ip","127.0.0.1");
        paramMap.put("notify_url","http://guli.shop/api/order/weixinPay/weixinNotify");
        paramMap.put("trade_type","NATIVE");

        httpClient.setXmlParam(WXPayUtil.generateSignedXml(paramMap, ConstantPropertiesUtils.PARTNERKEY));
        httpClient.setHttps(true);//支持https协议
        httpClient.post();//发送请求

        String content = httpClient.getContent();
        Map<String, String> stringStringMap = WXPayUtil.xmlToMap(content);

        return stringStringMap.get("code_url");
    }

    @Override
    public Map<String, String> getPayStatus(Long orderId) {

        OrderInfo orderInfo = orderInfoService.getById(orderId);

        HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
        Map<String, String> paramMap=new HashMap<String, String>();
        paramMap.put("appid",ConstantPropertiesUtils.APPID);
        paramMap.put("mch_id",ConstantPropertiesUtils.PARTNER);
        paramMap.put("out_trade_no",orderInfo.getOutTradeNo());

        paramMap.put("nonce_str",WXPayUtil.generateNonceStr());

        try {
            httpClient.setXmlParam(WXPayUtil.generateSignedXml(paramMap, ConstantPropertiesUtils.PARTNERKEY));
            httpClient.setHttps(true);
            httpClient.post();
            Map<String, String> stringStringMap = WXPayUtil.xmlToMap(httpClient.getContent());
            return stringStringMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Transactional
    @Override
    public void updateStatus(Long orderId, Map<String, String> map) {
        //1.更新订单表的订单状态
        OrderInfo orderInfo=new OrderInfo();
        orderInfo.setId(orderId);
        orderInfo.setOrderStatus(OrderStatusEnum.PAID.getStatus());
        orderInfoService.updateById(orderInfo);
       //2.更新支付记录表的支付状态
        PaymentInfo paymentInfo=new PaymentInfo();
        paymentInfo.setTradeNo(map.get("transaction_id"));
        paymentInfo.setCallbackTime(new Date());
        paymentInfo.setCallbackContent(map.toString());
        paymentInfo.setPaymentStatus(PaymentStatusEnum.PAID.getStatus());

        UpdateWrapper<PaymentInfo> updateWrapper=new UpdateWrapper<PaymentInfo>();
        updateWrapper.eq("order_id", orderId);
        paymentInfoService.update(paymentInfo,updateWrapper);

        //3.
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("hoscode",orderInfo.getHoscode());
        paramMap.put("hosRecordId",orderInfo.getHosRecordId());
        HttpRequestHelper.sendRequest(paramMap,"http://localhost:9998/order/updatePayStatus");
    }

    @Override
    public boolean refund(Long orderId) {
        QueryWrapper<PaymentInfo> queryWrapper=new QueryWrapper<PaymentInfo>();
        queryWrapper.eq("order_id",orderId);
        PaymentInfo paymentInfo = paymentInfoService.getOne(queryWrapper);

        //1.保存一条退款记录
        RefundInfo refundInfo=refundInfoService.saveRefundInfo(paymentInfo);

        if(refundInfo.getRefundStatus().intValue()== RefundStatusEnum.REFUND.getStatus()){
            return true;
        }


        //完成微信退款-->请求微信服务器
        HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/secapi/pay/refund");

        Map<String,String> paramMap=new HashMap<String,String>();
        paramMap.put("appid",ConstantPropertiesUtils.APPID);
        paramMap.put("mch_id",ConstantPropertiesUtils.PARTNER);
        paramMap.put("nonce_str",WXPayUtil.generateNonceStr());
        paramMap.put("transaction_id",paymentInfo.getTradeNo());
        paramMap.put("out_refund_no","tk"+paymentInfo.getOutTradeNo());

        paramMap.put("total_fee","1");
        paramMap.put("refund_fee","1");

        try {
            httpClient.setXmlParam(WXPayUtil.generateSignedXml(paramMap,ConstantPropertiesUtils.PARTNERKEY));

            httpClient.setCert(true);
            httpClient.setCertPassword(ConstantPropertiesUtils.PARTNER);

            httpClient.setHttps(true);
            httpClient.post();

            String content = httpClient.getContent();
            Map<String, String> stringStringMap = WXPayUtil.xmlToMap(content);
            if(stringStringMap.get("result_code").equals(WXPayConstants.SUCCESS)){

                refundInfo.setTradeNo(stringStringMap.get("refund_id"));
                refundInfo.setRefundStatus(RefundStatusEnum.REFUND.getStatus());
                refundInfo.setCallbackTime(new Date());
                refundInfo.setCallbackContent(stringStringMap.toString());

                refundInfoService.updateById(refundInfo);
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
