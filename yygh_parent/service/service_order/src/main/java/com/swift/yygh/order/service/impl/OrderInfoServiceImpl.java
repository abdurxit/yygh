package com.swift.yygh.order.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.swift.yygh.common.exception.MyYyghException;
import com.swift.yygh.enums.OrderStatusEnum;
import com.swift.yygh.enums.PaymentStatusEnum;
import com.swift.yygh.hosp.client.ScheduleFeignClient;
import com.swift.yygh.model.order.OrderInfo;
import com.swift.yygh.model.order.PaymentInfo;
import com.swift.yygh.model.user.Patient;
import com.swift.yygh.mq.constant.MqConst;
import com.swift.yygh.order.mapper.OrderInfoMapper;
import com.swift.yygh.order.service.OrderInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.order.service.PaymentInfoService;
import com.swift.yygh.order.service.WeiPayService;
import com.swift.yygh.order.utils.HttpRequestHelper;
import com.swift.yygh.user.client.PatientFeignClient;
import com.swift.yygh.vo.hosp.ScheduleOrderVo;
import com.swift.yygh.vo.msm.MsmVo;
import com.swift.yygh.vo.order.OrderCountQueryVo;
import com.swift.yygh.vo.order.OrderCountVo;
import com.swift.yygh.vo.order.OrderMqVo;
import com.swift.yygh.vo.order.OrderQueryVo;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author swift
 * @since 2023-01-29
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {

    @Autowired
    private PatientFeignClient patientFeignClient;

    @Autowired
    private ScheduleFeignClient scheduleFeignClient;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private WeiPayService weiPayService;

    @Autowired
    private PaymentInfoService paymentInfoService;
    @Override
    public Long submitOrder(Long patientId, String scheduleId) {

            OrderInfo orderInfo=new OrderInfo();
            //1.根据就诊人id获取就诊人信息
            Patient patient = patientFeignClient.list(patientId);
            //2.根据排班id获取排班信息
            ScheduleOrderVo scheduleOrderVo = scheduleFeignClient.remote(scheduleId);

            //3.在平台给第三方医院发送请求
            Map<String,Object> paramMap=new HashMap<String,Object>();
            paramMap.put("hoscode", scheduleOrderVo.getHoscode());
            paramMap.put("depcode", scheduleOrderVo.getDepcode());
            paramMap.put("hosScheduleId", scheduleOrderVo.getHosScheduleId());
            paramMap.put("reserveDate", scheduleOrderVo.getReserveDate());
            paramMap.put("reserveTime", scheduleOrderVo.getReserveTime());
            paramMap.put("amount", scheduleOrderVo.getAmount());

            JSONObject response = HttpRequestHelper.sendRequest(paramMap, "http://localhost:9998/order/submitOrder");
            if(response != null && response.getInteger("code").intValue() == 200){
                //3.2 第三方医院允许预约
                JSONObject data = response.getJSONObject("data");
                String hosRecordId = data.getString("hosRecordId");
                Integer number = data.getInteger("number");
                String fetchTime = data.getString("fetchTime");
                String fetchAddress = data.getString("fetchAddress");
                Integer reservedNumber = data.getInteger("reservedNumber");
                Integer availableNumber = data.getInteger("availableNumber");

                //3.2.1 把上面的三部分数据[就诊人、医生排班、第三方医院返回信息]插入到order_info表中

                orderInfo.setUserId(patient.getUserId());
                String outTradeNo  = System.currentTimeMillis() + "" + new Random().nextInt(100);
                orderInfo.setOutTradeNo(outTradeNo);

                BeanUtils.copyProperties(scheduleOrderVo,orderInfo);
                orderInfo.setScheduleId(scheduleOrderVo.getHosScheduleId());

                orderInfo.setPatientId(patient.getId());
                orderInfo.setPatientName(patient.getName());
                orderInfo.setPatientPhone(patient.getPhone());
                orderInfo.setHosRecordId(hosRecordId);
                orderInfo.setNumber(number);
                orderInfo.setFetchTime(fetchTime);
                orderInfo.setFetchAddress(fetchAddress);
                orderInfo.setOrderStatus(OrderStatusEnum.UNPAID.getStatus());

                baseMapper.insert(orderInfo);
                //3.2.2 更新平台上该医生的剩余可预约数

                OrderMqVo orderMqVo=new OrderMqVo();
                orderMqVo.setScheduleId(scheduleId);
                orderMqVo.setAvailableNumber(availableNumber);

                MsmVo msmVo=new MsmVo();
                msmVo.setPhone(patient.getPhone());
                msmVo.setTemplateCode("SMS_247905059");

                Map<String,Object> map=new HashMap<String,Object>();
                map.put("submittime",scheduleOrderVo.getReserveDate());
                map.put("name",patient.getName());
                msmVo.setParam(map);

                orderMqVo.setMsmVo(msmVo);

                rabbitTemplate.convertAndSend(MqConst.EXCHANGE_ORDER,MqConst.ROUTING_KEY_ORDER,orderMqVo);
                //3.2.3 给就诊人发送短信提示信息
            }else{
                //3.1 第三方医院不允许预约，直接抛出异常
                throw new MyYyghException(2001,"预约失败");
            }
            //4.将生成的订单id返回
            return orderInfo.getId();
        }

    @Override
    public Page<OrderInfo> selectPage(Integer pageNum, Integer pageSize, OrderQueryVo orderQueryVo) {
        Page<OrderInfo> page=new Page<OrderInfo>(pageNum,pageSize);

        QueryWrapper<OrderInfo> wrapper=new QueryWrapper<OrderInfo>();

        String name = orderQueryVo.getKeyword(); //医院名称
        Long patientId = orderQueryVo.getPatientId(); //就诊人名称
        String orderStatus = orderQueryVo.getOrderStatus(); //订单状态
        String reserveDate = orderQueryVo.getReserveDate();//安排时间
        String createTimeBegin = orderQueryVo.getCreateTimeBegin();
        String createTimeEnd = orderQueryVo.getCreateTimeEnd();
        Long userId = orderQueryVo.getUserId();

        if(!StringUtils.isEmpty(userId)) {
            wrapper.eq("user_id",userId);
        }

        if(!StringUtils.isEmpty(name)) {
            wrapper.like("hosname",name);
        }
        if(!StringUtils.isEmpty(patientId)) {
            wrapper.eq("patient_id",patientId);
        }
        if(!StringUtils.isEmpty(orderStatus)) {
            wrapper.eq("order_status",orderStatus);
        }
        if(!StringUtils.isEmpty(reserveDate)) {
            wrapper.ge("reserve_date",reserveDate);
        }
        if(!StringUtils.isEmpty(createTimeBegin)) {
            wrapper.ge("create_time",createTimeBegin);
        }
        if(!StringUtils.isEmpty(createTimeEnd)) {
            wrapper.le("create_time",createTimeEnd);
        }

        Page<OrderInfo> orderInfoPage = baseMapper.selectPage(page, wrapper);
        for (OrderInfo record : orderInfoPage.getRecords()) {
            packageOrderInfo(record);
        }
        return orderInfoPage;
    }

    @Override
    public OrderInfo getOrderByOrderId(Long orderId) {
        OrderInfo orderInfo = baseMapper.selectById(orderId);
        this.packageOrderInfo(orderInfo);
        return orderInfo;
    }

    @Override
    public void cancelOrder(Long orderId) {
        //1.先比较当前时间和订单的退号截止时间
        OrderInfo orderInfo = baseMapper.selectById(orderId);
        DateTime quitTime = new DateTime(orderInfo.getQuitTime());
        //1.1 当前时间 超过了 订单的退号截止时间，直接抛出异常
        if(quitTime.isBeforeNow()){
            throw new MyYyghException(2001,"超过了退号截止时间");
        }

        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("hoscode", orderInfo.getHoscode());
        paramMap.put("hosRecordId", orderInfo.getHosRecordId());

        //2.平台给第三方医院发请求，通知第三方医院
        JSONObject jsonObject = HttpRequestHelper.sendRequest(paramMap, "http://localhost:9998/order/updateCancelStatus");

        if(jsonObject !=null && jsonObject.getIntValue("code")== 200){
            //3.判断当前订单的订单状态是否为已支付
            if(orderInfo.getOrderStatus().intValue() == OrderStatusEnum.PAID.getStatus().intValue()){
                //3.1 已支付：微信退款
                boolean flag= weiPayService.refund(orderId);
                if(!flag){
                    throw new MyYyghException(2001,"退款失败");
                }
            }

        }else{//2.1 第三方医院不允许取消预约，直接抛出异常
            throw new MyYyghException(2001,"第三方医院不允许取消");
        }

        //4.更改订单表的订单状态和支付记录表的支付记录状态
        orderInfo.setOrderStatus(OrderStatusEnum.CANCLE.getStatus());
        baseMapper.updateById(orderInfo);

        UpdateWrapper<PaymentInfo> updateWrapper=new UpdateWrapper<PaymentInfo>();
        updateWrapper.eq("order_id",orderInfo.getId());


        PaymentInfo paymentInfo=new PaymentInfo();
        paymentInfo.setPaymentStatus(PaymentStatusEnum.REFUND.getStatus());
        paymentInfoService.update(paymentInfo,updateWrapper);


        //5.更新平台上该医生的剩余可预约数+1
        OrderMqVo orderMqVo=new OrderMqVo();
        orderMqVo.setScheduleId(orderInfo.getHoscode()+":"+orderInfo.getDepcode()+":"+orderInfo.getScheduleId());


        MsmVo msmVo=new MsmVo();
        msmVo.setPhone(orderInfo.getPatientPhone());
        msmVo.setTemplateCode("hello 你的预约已经取消了");
        orderMqVo.setMsmVo(msmVo);
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_ORDER,MqConst.ROUTING_KEY_ORDER,orderMqVo);



    }

    @Override
    public void consume() {
        QueryWrapper<OrderInfo> queryWrapper=new QueryWrapper<OrderInfo>();
        queryWrapper.eq("reserve_date", new DateTime().toString("yyyy-MM-dd"));
        queryWrapper.ne("order_status",OrderStatusEnum.CANCLE.getStatus());
        List<OrderInfo> orderInfos = baseMapper.selectList(queryWrapper);
        for (OrderInfo orderInfo : orderInfos) {

            String patientPhone = orderInfo.getPatientPhone();

            MsmVo msmVo=new MsmVo();
            msmVo.setPhone(patientPhone);
            msmVo.setTemplateCode("hello this is 医院挂号平台提高服务。。。");

            rabbitTemplate.convertAndSend(MqConst.EXCHANGE_SMS,MqConst.ROUTING_KEY_SMS,msmVo);
        }
    }

    @Override
    public Map<String, Object> getStatisticsData(OrderCountQueryVo orderCountQueryVo) {
        List<OrderCountVo> orderCountVoList=baseMapper.selectCountVoList(orderCountQueryVo);

//        List<String> dateList=new ArrayList<String>();
//        List<Integer> countList=new ArrayList<Integer>();
//
//        for (OrderCountVo countVo : orderCountVoList) {
//            String reserveDate = countVo.getReserveDate();
//            dateList.add(reserveDate);
//            countList.add(countVo.getCount());
//        }

        List<String> dateList = orderCountVoList.stream().map(item -> item.getReserveDate()).collect(Collectors.toList());
        List<Integer> countList = orderCountVoList.stream().map(item -> item.getCount()).collect(Collectors.toList());

        Map<String,Object> resultMap=new HashMap<String,Object>();
        resultMap.put("dateList",dateList);
        resultMap.put("countList",countList);

        return resultMap;
    }

    private void packageOrderInfo(OrderInfo record) {
        record.getParam().put("orderStatusString", OrderStatusEnum.getStatusNameByStatus(record.getOrderStatus()));
    }
}

