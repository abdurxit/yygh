package com.swift.yygh.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.enums.PaymentTypeEnum;
import com.swift.yygh.enums.RefundStatusEnum;
import com.swift.yygh.model.order.PaymentInfo;
import com.swift.yygh.model.order.RefundInfo;
import com.swift.yygh.order.mapper.RefundInfoMapper;
import com.swift.yygh.order.service.RefundInfoService;
import org.springframework.stereotype.Service;

@Service
public class refundInfoServiceImpl extends ServiceImpl<RefundInfoMapper, RefundInfo> implements RefundInfoService {
    @Override
    public RefundInfo saveRefundInfo(PaymentInfo paymentInfo) {

        QueryWrapper<RefundInfo> queryWrapper=new QueryWrapper<RefundInfo>();
        queryWrapper.eq("order_id", paymentInfo.getOrderId());
        RefundInfo refundInfo = baseMapper.selectOne(queryWrapper);
        if(refundInfo != null){
            return refundInfo;
        }

        refundInfo=new RefundInfo();
        refundInfo.setOutTradeNo(paymentInfo.getOutTradeNo());
        refundInfo.setOrderId(paymentInfo.getOrderId());
        refundInfo.setPaymentType(PaymentTypeEnum.WEIXIN.getStatus());
        refundInfo.setTotalAmount(paymentInfo.getTotalAmount());
        refundInfo.setSubject("欢迎使用尚一通医院挂号平台");
        refundInfo.setRefundStatus(RefundStatusEnum.UNREFUND.getStatus());

        baseMapper.insert(refundInfo);
        return refundInfo;
    }
}
