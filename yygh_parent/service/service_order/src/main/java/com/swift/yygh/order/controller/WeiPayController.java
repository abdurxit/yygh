package com.swift.yygh.order.controller;


import com.swift.yygh.common.result.R;
import com.swift.yygh.order.service.WeiPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/user/weipay")
public class WeiPayController {

    @Autowired
    private WeiPayService weiPayService;

    @GetMapping("/{orderId}")
    public R createNative(@PathVariable Long orderId) throws Exception {
       String url= weiPayService.createNative(orderId);
       return R.ok().data("url",url);
    }

    @GetMapping("/payStatus/{orderId}")
    public R getPayStatus(@PathVariable Long orderId) throws Exception {
        Map<String,String> map= weiPayService.getPayStatus(orderId);
        if(map== null){
            return R.error().message("查询失败");
        }
        if(map.get("trade_state").equalsIgnoreCase("SUCCESS")){
            //修改订单表的订单状态、支付记录表的支付状态、通知第三方医院
            weiPayService.updateStatus(orderId,map);
            return R.ok().message("支付成功");
        }
        return R.ok().message("支付中");
    }


}
