package com.swift.yygh.order.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.enums.PaymentStatusEnum;
import com.swift.yygh.enums.PaymentTypeEnum;
import com.swift.yygh.model.order.OrderInfo;
import com.swift.yygh.model.order.PaymentInfo;
import com.swift.yygh.order.mapper.PaymentInfoMapper;
import com.swift.yygh.order.service.PaymentInfoService;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.stereotype.Service;


@Service
public class PaymentInfoServiceImpl extends ServiceImpl<PaymentInfoMapper, PaymentInfo> implements PaymentInfoService {

    @Override
    public void savePaymentInfo(OrderInfo orderInfo) {

        QueryWrapper<PaymentInfo> queryWrapper=new QueryWrapper<PaymentInfo>();
        queryWrapper.eq("order_id",orderInfo.getId());
        PaymentInfo paymentInfo1 = baseMapper.selectOne(queryWrapper);
        if(paymentInfo1 != null){
            return;
        }

        PaymentInfo paymentInfo=new PaymentInfo();
        paymentInfo.setOutTradeNo(orderInfo.getOutTradeNo());
        paymentInfo.setOrderId(orderInfo.getId());
        paymentInfo.setPaymentType(PaymentTypeEnum.WEIXIN.getStatus());
        paymentInfo.setTotalAmount(orderInfo.getAmount());

        paymentInfo.setSubject("有钱任性.....");
        paymentInfo.setPaymentStatus(PaymentStatusEnum.UNPAID.getStatus());

        baseMapper.insert(paymentInfo);
    }
}
