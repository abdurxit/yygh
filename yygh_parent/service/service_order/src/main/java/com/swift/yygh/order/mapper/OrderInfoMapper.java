package com.swift.yygh.order.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.order.OrderInfo;
import com.swift.yygh.vo.order.OrderCountQueryVo;
import com.swift.yygh.vo.order.OrderCountVo;

import java.util.List;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author swift
 * @since 2023-01-29
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {


    List<OrderCountVo> selectCountVoList(OrderCountQueryVo orderCountQueryVo);
}
