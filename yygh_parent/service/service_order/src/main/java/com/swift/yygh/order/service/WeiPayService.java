package com.swift.yygh.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.order.PaymentInfo;

import java.util.Map;

public interface WeiPayService  {
    String createNative(Long orderId) throws Exception;

    Map<String, String> getPayStatus(Long orderId);

    void updateStatus(Long orderId, Map<String, String> map);

    boolean refund(Long orderId);
}
