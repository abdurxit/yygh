package com.swift.yygh.order.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.order.OrderInfo;
import com.swift.yygh.vo.order.OrderCountQueryVo;
import com.swift.yygh.vo.order.OrderQueryVo;

import java.util.Map;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author swift
 * @since 2023-01-29
 */
public interface OrderInfoService extends IService<OrderInfo> {

    Long submitOrder(Long patientId, String scheduleId);

    Page<OrderInfo> selectPage(Integer pageNum, Integer pageSize, OrderQueryVo orderQueryVo);

    OrderInfo getOrderByOrderId(Long orderId);

    void cancelOrder(Long orderId);

    void consume();

    Map<String, Object> getStatisticsData(OrderCountQueryVo orderCountQueryVo);
}
