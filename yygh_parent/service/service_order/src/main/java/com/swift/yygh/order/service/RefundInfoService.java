package com.swift.yygh.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.order.PaymentInfo;
import com.swift.yygh.model.order.RefundInfo;

public interface RefundInfoService extends IService<RefundInfo> {
    RefundInfo saveRefundInfo(PaymentInfo paymentInfo);
}
