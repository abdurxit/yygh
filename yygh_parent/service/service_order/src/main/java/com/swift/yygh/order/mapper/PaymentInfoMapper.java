package com.swift.yygh.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.order.PaymentInfo;


public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {
}
