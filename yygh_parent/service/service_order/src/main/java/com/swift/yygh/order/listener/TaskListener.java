package com.swift.yygh.order.listener;

import com.rabbitmq.client.Channel;
import com.swift.yygh.mq.constant.MqConst;
import com.swift.yygh.order.service.OrderInfoService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskListener {

    @Autowired
    private OrderInfoService orderInfoService;
    @RabbitListener(queues = MqConst.QUEUE_TASK)
    public void consume(Message message , Channel channel){
        orderInfoService.consume();
    }
}
