package com.swift.yygh.order.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.order.OrderInfo;
import com.swift.yygh.model.order.PaymentInfo;


public interface PaymentInfoService extends IService<PaymentInfo> {
    void savePaymentInfo(OrderInfo orderInfo);
}
