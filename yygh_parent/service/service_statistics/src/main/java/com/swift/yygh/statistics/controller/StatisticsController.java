package com.swift.yygh.statistics.controller;

import com.swift.yygh.common.result.R;
import com.swift.yygh.order.client.OrderInfoFeignClient;
import com.swift.yygh.vo.order.OrderCountQueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/admin/statistics")
public class StatisticsController {

    @Autowired
    private OrderInfoFeignClient orderInfoFeignClient;

    @PostMapping("/info")
    public R getStatisticInfo(@RequestBody OrderCountQueryVo orderCountQueryVo){
        Map<String, Object> statisticsData = orderInfoFeignClient.getStatisticsData(orderCountQueryVo);
        return R.ok().data(statisticsData);
    }


}
