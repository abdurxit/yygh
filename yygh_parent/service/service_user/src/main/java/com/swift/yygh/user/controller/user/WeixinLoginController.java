package com.swift.yygh.user.controller.user;

import com.alibaba.fastjson.JSONObject;
import com.swift.yygh.common.exception.MyYyghException;
import com.swift.yygh.common.result.R;
import com.swift.yygh.common.utils.JwtHelper;
import com.swift.yygh.model.user.UserInfo;
import com.swift.yygh.user.prop.WeixinProperties;
import com.swift.yygh.user.service.UserInfoService;
import com.swift.yygh.user.utils.HttpClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping("/user/weixin")
public class WeixinLoginController {

    @Autowired
    private WeixinProperties weixinProperties;
    @Autowired
    private UserInfoService userInfoService;


    @GetMapping("/param")
    @ResponseBody
    public R getParam() throws Exception {
        Map<String,Object> paramMap=new HashMap<String,Object>();
        paramMap.put("appid",weixinProperties.getAppid());
        paramMap.put("scope","snsapi_login");
        paramMap.put("redirect_uri",URLEncoder.encode(weixinProperties.getRedirectUrl(), "UTF-8"));
        paramMap.put("state",System.currentTimeMillis()+"");
        return R.ok().data(paramMap);
    }

    @GetMapping("/callback")
    public String callback(String code,String state) throws Exception {


        StringBuilder stringBuilder=new StringBuilder();
        StringBuilder append = stringBuilder.append("https://api.weixin.qq.com/sns/oauth2/access_token")
                .append("?appid=%s")
                .append("&secret=%s")
                .append("&code=%s")
                .append("&grant_type=authorization_code");

        String format = String.format(append.toString(), weixinProperties.getAppid(), weixinProperties.getSecret(), code);

        JSONObject jsonObject = JSONObject.parseObject(HttpClientUtils.get(format));
        String access_token = jsonObject.getString("access_token");
        String openid = jsonObject.getString("openid");
        UserInfo userInfo=userInfoService.getUserInfoByOpenId(openid);
        if(userInfo == null){
            String s = HttpClientUtils.get("https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + openid);
            System.out.println(s);
            JSONObject userInfoObject = JSONObject.parseObject(s);

            String nickname = userInfoObject.getString("nickname");

            userInfo=new UserInfo();
            userInfo.setOpenid(openid);
            userInfo.setStatus(1);
            userInfo.setNickName(nickname);
            userInfoService.save(userInfo);
        }

        if(userInfo.getStatus().intValue() == 0){
            throw new MyYyghException(20001,"该用户已被禁用");
        }

        Map<String,String> resultMap=new HashMap<String,String>();
        String name = userInfo.getName();
        if(StringUtils.isEmpty(name)){
            name=userInfo.getNickName();
        }

        String phone = userInfo.getPhone();
        if(StringUtils.isEmpty(phone)){
            resultMap.put("openid",openid); //openid有值，要做手机号绑定
        }else{
            resultMap.put("openid","");
        }

        resultMap.put("name",name);
        resultMap.put("token", JwtHelper.createToken(userInfo.getId(),name));

        return "redirect:http://localhost:3000/weixin/callback?name="+URLEncoder.encode(resultMap.get("name"),"utf-8")+"&token="+resultMap.get("token")+"&openid="+resultMap.get("openid");
    }


}

