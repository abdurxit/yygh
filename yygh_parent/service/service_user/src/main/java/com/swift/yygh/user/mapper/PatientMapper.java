package com.swift.yygh.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.user.Patient;

/**
 * <p>
 * 就诊人表 Mapper 接口
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
public interface PatientMapper extends BaseMapper<Patient> {

}
