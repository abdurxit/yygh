package com.swift.yygh.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.swift.yygh.common.exception.MyYyghException;
import com.swift.yygh.common.utils.JwtHelper;
import com.swift.yygh.enums.AuthStatusEnum;
import com.swift.yygh.enums.StatusEnum;
import com.swift.yygh.model.user.Patient;
import com.swift.yygh.model.user.UserInfo;
import com.swift.yygh.user.mapper.UserInfoMapper;
import com.swift.yygh.user.service.PatientService;
import com.swift.yygh.user.service.UserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.vo.user.LoginVo;
import com.swift.yygh.vo.user.UserAuthVo;
import com.swift.yygh.vo.user.UserInfoQueryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {


    @Autowired
    private PatientService patientService;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Override
    public Map<String, Object> login(LoginVo loginVo) {
        UserInfo userInfo=null;
        //1.获取前端传过来的手机号和验证码
        String phone = loginVo.getPhone();
        String code = loginVo.getCode();

        //2.对手机号和验证码进行非空判断
        if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(code)){
            throw new MyYyghException(20001,"手机号、验证码不能为空");
        }

        //3.TODO: 对验证码进行合法性判断
        String redisCode = redisTemplate.opsForValue().get(phone);
        if(StringUtils.isEmpty(redisCode) || !redisCode.equals(code)){
            throw new MyYyghException(20001,"验证码有误");
        }

        //4.判断当前用户是否是首次登录，如果是，做注册
        if(StringUtils.isEmpty(loginVo.getOpenid())){
            //4.判断当前用户是否是首次登录，如果是，做注册
            QueryWrapper<UserInfo> queryWrapper=new QueryWrapper<UserInfo>();
            queryWrapper.eq("phone",phone);
            userInfo = baseMapper.selectOne(queryWrapper);
            if(userInfo == null){
                userInfo=new UserInfo();
                userInfo.setPhone(phone);
                userInfo.setStatus(1);
                baseMapper.insert(userInfo);
            }
        }else{//微信强制绑定手机号登录
            QueryWrapper<UserInfo> queryWrapper=new QueryWrapper<UserInfo>();
            queryWrapper.eq("openid",loginVo.getOpenid());
            userInfo = baseMapper.selectOne(queryWrapper);

            QueryWrapper<UserInfo> queryWrapper2=new QueryWrapper<UserInfo>();
            queryWrapper2.eq("phone",phone);
            UserInfo userInfo2 = baseMapper.selectOne(queryWrapper2);

            if(userInfo2 == null){
                userInfo.setPhone(phone);
                baseMapper.updateById(userInfo);
            }else{
                userInfo2.setOpenid(userInfo.getOpenid());
                userInfo2.setNickName(userInfo.getNickName());
                baseMapper.updateById(userInfo2);
                baseMapper.deleteById(userInfo.getId());
                BeanUtils.copyProperties(userInfo2,userInfo);
            }
        }


        //5.判断用户的status,如果用户已经是锁定状态，直接抛出异常
        if(userInfo.getStatus().intValue() == 0){
            throw new MyYyghException(20001,"该用户已锁定");
        }

        //6.返回给前端用户信息
        Map<String, Object> resultMap=new HashMap<String, Object>();
        String name = userInfo.getName();
        if(StringUtils.isEmpty(name)){
            name=userInfo.getNickName();
        }
        if(StringUtils.isEmpty(name)){
            name=userInfo.getPhone();
        }

        resultMap.put("name",name);
        String token = JwtHelper.createToken(userInfo.getId().toString(), name);
        resultMap.put("token",token);
        return resultMap;
    }

    @Override
    public UserInfo getUserInfoByOpenId(String openid) {
        QueryWrapper<UserInfo> queryWrapper=new QueryWrapper<UserInfo>();
        queryWrapper.eq("openid",openid);
        UserInfo userInfo = baseMapper.selectOne(queryWrapper);
        return userInfo;
    }

    @Override
    public UserInfo getUserInfo(Long userId) {
        UserInfo userInfo = baseMapper.selectById(userId);
        userInfo.getParam().put("authStatusString", AuthStatusEnum.getStatusNameByStatus(userInfo.getAuthStatus()));
        return userInfo;
    }

    @Override
    public void updateAuthStatus(UserAuthVo userAuthVo, Long userId) {
        UserInfo userInfo = baseMapper.selectById(userId);
        userInfo.setName(userAuthVo.getName());
        userInfo.setCertificatesType(userAuthVo.getCertificatesType());
        userInfo.setCertificatesNo(userAuthVo.getCertificatesNo());
        userInfo.setCertificatesUrl(userAuthVo.getCertificatesUrl());
        userInfo.setAuthStatus(AuthStatusEnum.AUTH_RUN.getStatus());
        baseMapper.updateById(userInfo);
    }

    @Override
    public Page<UserInfo> selectPage(Integer pageNum, Integer pageSize, UserInfoQueryVo userInfoQueryVo) {
        QueryWrapper<UserInfo> queryWrapper=new QueryWrapper<UserInfo>();

        String keyword = userInfoQueryVo.getKeyword(); //用户名称|手机号
        Integer status = userInfoQueryVo.getStatus();//用户状态
        Integer authStatus = userInfoQueryVo.getAuthStatus(); //认证状态
        String createTimeBegin = userInfoQueryVo.getCreateTimeBegin(); //开始时间
        String createTimeEnd = userInfoQueryVo.getCreateTimeEnd(); //结束时间

        if(!StringUtils.isEmpty(keyword)){
            queryWrapper.and(qr->qr.like("name",keyword).or().eq("phone",keyword));
        }
        if(!StringUtils.isEmpty(status)){
            queryWrapper.eq("status",status);
        }
        if(!StringUtils.isEmpty(authStatus)){
            queryWrapper.eq("auth_status",authStatus);
        }
        if(!StringUtils.isEmpty(createTimeBegin)){
            queryWrapper.ge("create_time",createTimeBegin);
        }
        if(!StringUtils.isEmpty(createTimeEnd)){
            queryWrapper.le("create_time",createTimeEnd);
        }

        Page<UserInfo> page=new Page<UserInfo>(pageNum,pageSize);
        baseMapper.selectPage(page, queryWrapper);
        for (UserInfo record : page.getRecords()) {
            record.getParam().put("authStatusString",AuthStatusEnum.getStatusNameByStatus(record.getAuthStatus()));
            record.getParam().put("statusString", StatusEnum.getDescByCode(record.getStatus()));
        }
        return page;
    }

    @Override
    public void lock(Long userId, Integer status) {
        if(status.intValue() == 0 || status.intValue() == 1) {
            UserInfo userInfo = this.getById(userId);
            userInfo.setStatus(status);
            this.updateById(userInfo);
        }
    }

    @Override
    public Map<String, Object> show(Long userId) {
        Map<String, Object> resultMap=new HashMap<String, Object>();

        UserInfo userInfo = baseMapper.selectById(userId);
        this.packageUserInfo(userInfo);
        resultMap.put("userInfo",userInfo);


        List<Patient> patients = patientService.getPatientListByUserId(userId);
        resultMap.put("patientList",patients);
        return resultMap;
    }

    @Override
    public void approval(Long userId, Integer authStatus) {
        if(authStatus.intValue()==2 || authStatus.intValue()==-1) {
            UserInfo userInfo = baseMapper.selectById(userId);
            userInfo.setAuthStatus(authStatus);
            baseMapper.updateById(userInfo);
        }
    }

    private void packageUserInfo(UserInfo record) {
        record.getParam().put("authStatusString", AuthStatusEnum.getStatusNameByStatus(record.getAuthStatus()));
        record.getParam().put("statusString", StatusEnum.getDescByCode(record.getStatus()));
    }
}
