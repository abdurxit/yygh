package com.swift.yygh.user.controller.user;


import com.swift.yygh.common.result.R;
import com.swift.yygh.common.utils.JwtHelper;
import com.swift.yygh.model.user.Patient;
import com.swift.yygh.user.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 就诊人表 前端控制器
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
@RestController
@RequestMapping("/user/patient")
public class PatientController {


    @Autowired
    private PatientService patientService;

    //增
    @PostMapping("/save")
    public R save(@RequestBody Patient patient, @RequestHeader String token){
        Long userId = JwtHelper.getUserId(token);
        patient.setUserId(userId);
        patientService.save(patient);
        return R.ok();
    }
    //删
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable Long id){
        patientService.removeById(id);
        return R.ok();
    }
    //改:回显
    @GetMapping("/edit/{id}")
    public R edit(@PathVariable Long id){
        Patient patient = patientService.edit(id);
        return R.ok().data("patient",patient);
    }


    //改:
    @PutMapping("/update")
    public R update(@RequestBody Patient patient){
        patientService.updateById(patient);
        return R.ok();
    }

    //查
    @GetMapping("/list")
    public R list(@RequestHeader String token){
        Long userId = JwtHelper.getUserId(token);
        List<Patient> patients= patientService.getPatientListByUserId(userId);
        return R.ok().data("patients",patients);
    }


    @GetMapping("/remote/{id}")
    public Patient list(@PathVariable("id") Long id){
        return patientService.getById(id);
    }
}

