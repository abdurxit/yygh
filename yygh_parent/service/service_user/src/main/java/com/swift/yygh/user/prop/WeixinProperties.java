package com.swift.yygh.user.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import org.springframework.stereotype.Component;


@ConfigurationProperties(prefix = "weixin")
//@Component
@Data
public class WeixinProperties {

    private String appid;
    private String secret;
    private String redirectUrl;
}
