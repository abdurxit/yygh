package com.swift.yygh.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.swift.yygh.cmn.client.DictFeignClient;
import com.swift.yygh.model.user.Patient;
import com.swift.yygh.user.mapper.PatientMapper;
import com.swift.yygh.user.service.PatientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 就诊人表 服务实现类
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
@Service
public class PatientServiceImpl extends ServiceImpl<PatientMapper, Patient> implements PatientService {


    @Autowired
    private DictFeignClient dictFeignClient;

    @Override
    public Patient edit(Long id) {
        Patient patient = baseMapper.selectById(id);
        this.packagePatient(patient);
        return patient;
    }

    @Override
    public List<Patient> getPatientListByUserId(Long userId) {
        QueryWrapper<Patient> queryWrapper=new QueryWrapper<Patient>();
        queryWrapper.eq("user_id", userId);
        List<Patient> patients = baseMapper.selectList(queryWrapper);
        for (Patient patient : patients) {
            this.packagePatient(patient);
        }
        return patients;
    }

    private void packagePatient(Patient patient) {
        String certificatesType = patient.getCertificatesType();
        String certificatesTypeString = dictFeignClient.getName(Long.parseLong(certificatesType));
        patient.getParam().put("certificatesTypeString",certificatesTypeString);

        String provinceCode = patient.getProvinceCode();
        String cityCode = patient.getCityCode();
        String districtCode = patient.getDistrictCode();

        String provinceName = dictFeignClient.getName(Long.parseLong(provinceCode));
        String cityName = dictFeignClient.getName(Long.parseLong(cityCode));
        String districtName = dictFeignClient.getName(Long.parseLong(districtCode));

        patient.getParam().put("provinceString",provinceName);
        patient.getParam().put("cityString",cityName);
        patient.getParam().put("districtString",districtName);
        patient.getParam().put("fullAddress",provinceName+cityName+districtName+patient.getAddress());

    }


}