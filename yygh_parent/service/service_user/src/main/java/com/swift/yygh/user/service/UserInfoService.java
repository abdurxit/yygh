package com.swift.yygh.user.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.user.UserInfo;
import com.swift.yygh.vo.user.LoginVo;
import com.swift.yygh.vo.user.UserAuthVo;
import com.swift.yygh.vo.user.UserInfoQueryVo;

import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
public interface UserInfoService extends IService<UserInfo> {

    Map<String, Object> login(LoginVo loginVo);

    UserInfo getUserInfoByOpenId(String openid);

    UserInfo getUserInfo(Long userId);

    void updateAuthStatus(UserAuthVo userAuthVo, Long userId);

    Page<UserInfo> selectPage(Integer pageNum, Integer pageSize, UserInfoQueryVo userInfoQueryVo);

    void lock(Long userId, Integer status);

    Map<String, Object> show(Long userId);

    void approval(Long userId, Integer authStatus);
}
