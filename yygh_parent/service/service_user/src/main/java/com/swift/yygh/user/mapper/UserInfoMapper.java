package com.swift.yygh.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.user.UserInfo;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
