package com.swift.yygh.user.controller.user;


import com.swift.yygh.common.result.R;
import com.swift.yygh.common.utils.JwtHelper;
import com.swift.yygh.model.user.UserInfo;
import com.swift.yygh.user.service.UserInfoService;
import com.swift.yygh.vo.user.LoginVo;
import com.swift.yygh.vo.user.UserAuthVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
@RestController
@RequestMapping("/user/info")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @PostMapping("/login")
    public R login(@RequestBody LoginVo loginVo){
        Map<String,Object> map= userInfoService.login(loginVo);
        return R.ok().data(map);
    }

    @GetMapping("/info")
    public R getUserInfo(@RequestHeader String token){
        Long userId = JwtHelper.getUserId(token);
        UserInfo userInfo = userInfoService.getUserInfo(userId);
        return R.ok().data("userInfo",userInfo);
    }


    @PutMapping("/auth/status")
    public R updateAuthStatus(@RequestBody UserAuthVo userAuthVo, @RequestHeader String token){
        Long userId = JwtHelper.getUserId(token);
        userInfoService.updateAuthStatus(userAuthVo,userId);
        return R.ok();
    }
}

