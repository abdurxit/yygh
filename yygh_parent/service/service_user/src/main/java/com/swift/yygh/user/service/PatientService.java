package com.swift.yygh.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.user.Patient;

import java.util.List;

/**
 * <p>
 * 就诊人表 服务类
 * </p>
 *
 * @author swift
 * @since 2023-01-16
 */
public interface PatientService extends IService<Patient> {

    Patient edit(Long id);

    List<Patient> getPatientListByUserId(Long userId);

}
