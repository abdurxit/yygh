package com.swift.yygh.user.controller.admin;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.swift.yygh.common.result.R;
import com.swift.yygh.model.user.UserInfo;
import com.swift.yygh.user.service.UserInfoService;
import com.swift.yygh.vo.user.UserInfoQueryVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/admin/userinfo")
public class AdminUserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @PostMapping("/page/{pageNum}/{pageSize}")
    public R page(@PathVariable Integer pageNum,
                  @PathVariable Integer pageSize,
                  @RequestBody UserInfoQueryVo userInfoQueryVo){
       Page<UserInfo> page=userInfoService.selectPage(pageNum,pageSize,userInfoQueryVo);

       return R.ok().data("total",page.getTotal()).data("list",page.getRecords());
    }
    @ApiOperation(value = "锁定")
    @GetMapping("lock/{userId}/{status}")
    public R lock(
            @PathVariable("userId") Long userId,
            @PathVariable("status") Integer status){
        userInfoService.lock(userId, status);
        return R.ok();
    }

    @ApiOperation(value = "根据用户id查询用户信息")
    @GetMapping("show/{userId}")
    public R show(
            @PathVariable("userId") Long userId){
        Map<String,Object> map=userInfoService.show(userId);
        return R.ok().data(map);
    }

    //认证审批
    @PutMapping("approval/{userId}/{authStatus}")
    public R approval(@PathVariable Long userId,@PathVariable Integer authStatus) {
        userInfoService.approval(userId,authStatus);
        return R.ok();
    }

}
