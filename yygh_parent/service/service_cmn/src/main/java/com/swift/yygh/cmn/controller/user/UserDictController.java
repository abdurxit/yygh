package com.swift.yygh.cmn.controller.user;

import com.swift.yygh.cmn.service.DictService;
import com.swift.yygh.common.result.R;
import com.swift.yygh.model.cmn.Dict;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/user/cmn")
public class UserDictController {

    @Autowired
    private DictService dictService;

    @GetMapping("/dict/list/{dictCode}")
    public R getDictListByDictCode(@PathVariable String dictCode){
        List<Dict> dictList= dictService.findDictListByHoscode(dictCode);
        return R.ok().data("dictList",dictList);
    }


    @GetMapping("/children/{pid}")
    public R getChildrenByPid(@PathVariable Long pid){
        List<Dict> dictList= dictService.getDictListById(pid);
        return R.ok().data("list",dictList);
    }

}
