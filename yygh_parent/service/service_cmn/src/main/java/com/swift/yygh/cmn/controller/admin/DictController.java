package com.swift.yygh.cmn.controller.admin;


import com.swift.yygh.cmn.service.DictService;
import com.swift.yygh.common.result.R;
import com.swift.yygh.model.cmn.Dict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 组织架构表 前端控制器
 * </p>
 *
 * @author swift
 * @since 2023-01-04
 */
@Api(tags = "数据字典接口")
@RestController
@RequestMapping("/admin/cmn")
//@CrossOrigin
public class DictController {

    @Autowired
    private DictService dictService;

    @Cacheable(value = "dict")
    @ApiOperation("根据Id查询数据字典")
    @GetMapping("/dict/{pid}")
    public R getDictListById(@PathVariable("pid") Long pid){
       List<Dict> dictList = dictService.getDictListById(pid);
       return R.ok().data("list",dictList);
    }
    @ApiOperation(value = "文件下载接口")
    @GetMapping("/download")
    public void download( HttpServletResponse response) throws Exception {
        dictService.download(response);
    }


    @ApiOperation("文件上传接口")
    @PostMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws IOException {
        dictService.upload(file);
        return R.ok();
    }

    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "value", value = "值", required = true),
            @ApiImplicitParam(name = "parentDictCode", value = "上级编码", required = true)
    })
    @ApiOperation(value = "获取数据字典名称")
    @GetMapping(value = "/getName/{value}/{dictCode}")
    public String getName(@PathVariable("value") String value, @PathVariable("dictCode") String dictCode) {
        return dictService.getNameByValueAndDictCode(value,dictCode);
    }


     @ApiImplicitParam(name = "value", value = "值", required = true)
    @ApiOperation(value = "获取数据字典名称")
    @GetMapping(value = "/getName/{value}")
    public String getName(
        @PathVariable("value") String value) {
        return dictService.getNameByValue( value);
    }
}

