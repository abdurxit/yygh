package com.swift.yygh.cmn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.cmn.Dict;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface MyApiService extends IService<Dict> {
    List<Dict> getDictListById(Long parentId);

    void upload(MultipartFile file) throws IOException;

    void download(HttpServletResponse response) throws IOException;
}
