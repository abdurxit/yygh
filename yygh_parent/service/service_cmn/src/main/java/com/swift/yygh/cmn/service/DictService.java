package com.swift.yygh.cmn.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.cmn.Dict;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * <p>
 * 组织架构表 服务类
 * </p>
 *
 * @author swift
 * @since 2023-01-04
 */
public interface DictService extends IService<Dict> {

    List<Dict> getDictListById(Long pid);

    void download(HttpServletResponse response) throws Exception;

    void upload(MultipartFile file) throws IOException;

    String getNameByValue(String value);

    String getNameByValueAndDictCode(String value, String dictCode);

    List<Dict> findDictListByHoscode(String hoscode);

}
