package com.swift.yygh.cmn.controller;

import com.swift.yygh.cmn.service.MyApiService;
import com.swift.yygh.common.result.R;
import com.swift.yygh.model.cmn.Dict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Api(tags = "我的数据字典编程接口文档")
@RestController
@RequestMapping("/my")
public class MyApiController {

    @Autowired
    private MyApiService myApiService;

    @ApiOperation("根据ParentId查询父节点数据接口")
    @GetMapping("/getDictListById/{parentId}")
    @ApiImplicitParam(name = "parentId",value = "父节点的Id",required = true,dataType = "long",paramType = "path")
    public R getDictListById(@PathVariable("parentId") Long parentId){
     List<Dict> dictList =  myApiService.getDictListById(parentId);
        return R.ok().data("list",dictList);
    }

    @ApiOperation("上传")
    @PostMapping("/uploadOne")
    public R upload(@RequestParam("file")MultipartFile file) throws IOException {
        myApiService.upload(file);
        return R.ok();
    }


    @ApiOperation("下载")
    @GetMapping("/downloadOne")
    public void download(HttpServletResponse response) throws IOException {
        myApiService.download(response);
    }



























}
