package com.swift.yygh.cmn.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.swift.yygh.cmn.mapper.MyApiMapper;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.vo.cmn.DictEeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Slf4j
public class MyApiListener extends AnalysisEventListener<DictEeVo> {

    private MyApiMapper myApiMapper;

    public MyApiListener(MyApiMapper myApiMapper) {
        this.myApiMapper = myApiMapper;
    }

    @Override
    public void invoke(DictEeVo data, AnalysisContext context) {
      //  log.warn("从excl表中获取到的数据是 {}",data);
        Dict dict = new Dict();
        BeanUtils.copyProperties(data,dict);
        Dict dict1 = myApiMapper.selectById(dict.getId());
        if(dict1 == null){
            myApiMapper.insert(dict);
        }else {
            myApiMapper.updateById(dict);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
