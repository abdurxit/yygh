package com.swift.yygh.cmn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.model.hosp.HospitalSet;

public interface MyApi2Mapper extends BaseMapper<Dict> {
}
