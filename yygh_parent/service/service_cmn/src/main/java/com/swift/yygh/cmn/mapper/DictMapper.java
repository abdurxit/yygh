package com.swift.yygh.cmn.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.cmn.Dict;

/**
 * <p>
 * 组织架构表 Mapper 接口
 * </p>
 *
 * @author swift
 * @since 2023-01-04
 */
public interface DictMapper extends BaseMapper<Dict> {

}
