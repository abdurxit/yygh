package com.swift.yygh.cmn.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.swift.yygh.cmn.mapper.MyApi2Mapper;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.vo.cmn.DictEeVo;
import org.springframework.beans.BeanUtils;

public class MyApi2Listener extends AnalysisEventListener<DictEeVo> {

    private MyApi2Mapper myApi2Mapper;

    public MyApi2Listener(MyApi2Mapper myApi2Mapper) {
        this.myApi2Mapper = myApi2Mapper;
    }

    @Override
    public void invoke(DictEeVo data, AnalysisContext context) {
        Dict dict = new Dict();
        BeanUtils.copyProperties(data,dict);
        Dict dict1 = myApi2Mapper.selectById(dict.getId());
        if(dict1 == null){
            myApi2Mapper.insert(dict);
        }else {
            myApi2Mapper.updateById(dict);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
