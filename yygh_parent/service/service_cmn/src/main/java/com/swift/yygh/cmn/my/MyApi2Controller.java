package com.swift.yygh.cmn.my;

import com.swift.yygh.cmn.service.MyApi2Service;
import com.swift.yygh.common.result.R;
import com.swift.yygh.model.cmn.Dict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.channels.MulticastChannel;
import java.util.List;

@Api(tags = "我的接口")
@RestController
@RequestMapping("my")
public class MyApi2Controller {

    @Autowired
    private MyApi2Service myApi2Service;

    @ApiOperation("查询字典")
    @GetMapping("/findDictList/{parentId}")
    public R findDictList(@PathVariable("parentId") Long parentId){
       List<Dict> list =  myApi2Service.getDictList(parentId);
       return R.ok().data("list",list);
    }

    @ApiOperation("上传")
    @PostMapping("/uploadTwo")
    public R upload(@RequestParam("file")MultipartFile file) throws IOException {
        myApi2Service.upload(file);
        return R.ok();
    }
    @ApiOperation("下载")
    @GetMapping("、downloadTwo")
    public R download(HttpServletResponse response) throws IOException{
        myApi2Service.download(response);
        return R.ok();
    }

}
