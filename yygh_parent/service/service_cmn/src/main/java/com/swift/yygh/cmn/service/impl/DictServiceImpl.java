package com.swift.yygh.cmn.service.impl;


import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.swift.yygh.cmn.listener.DictListener;
import com.swift.yygh.cmn.mapper.DictMapper;
import com.swift.yygh.cmn.service.DictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.vo.cmn.DictEeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 组织架构表 服务实现类
 * </p>
 *
 * @author swift
 * @since 2023-01-04
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {

    @Override
    public List<Dict> getDictListById(Long pid) {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",pid);
        List<Dict> dictList = baseMapper.selectList(queryWrapper);
        for (Dict dict : dictList) {
            boolean result = isHasChildren(dict.getId());
            dict.setHasChildren(result);
        }
        return dictList;
    }


    private boolean isHasChildren(Long pid) {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",pid);
        Integer count = baseMapper.selectCount(queryWrapper);
        return count > 0 ;
    }



    @Override
    public void download(HttpServletResponse response) throws Exception {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("数据字典", "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        List<Dict> dictList = baseMapper.selectList(null);
        //创建dictVolist集合
        List<DictEeVo> dictEeVoList = new ArrayList<>();
        for (Dict dict : dictList) {
            //第一种方式 dict对象转换diceVo对象
          /*  DictEeVo dictEeVo = new DictEeVo();
            dictEeVo.setId(dict.getId());
            dictEeVo.setName(dict.getName());
            dictEeVo.setDictCode(dict.getDictCode());
            dictEeVo.setParentId(dict.getParentId());
            dictEeVo.setValue(dict.getValue());*/

          //第二中方式
            DictEeVo dictEeVo = new DictEeVo();
            BeanUtils.copyProperties(dict,dictEeVo);
            dictEeVoList.add(dictEeVo);
        }

        EasyExcel.write(response.getOutputStream(), DictEeVo.class).sheet("模板").doWrite(dictEeVoList);
    }

    @CacheEvict(value = "dict",allEntries = true)
    @Override
    public void upload(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(),DictEeVo.class,new DictListener(baseMapper)).sheet(0).doRead();
    }

    @Override
    public String getNameByValue(String value) {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("value",value);
        Dict dict = baseMapper.selectOne(queryWrapper);
        return dict.getName();
    }
    @Override
    public String getNameByValueAndDictCode(String value, String dictCode) {
        //如果value能唯一定位数据字典，parentDictCode可以传空，例如：省市区的value值能够唯一确定

        if(StringUtils.isEmpty(dictCode)) {

            Dict dict = baseMapper.selectOne(new QueryWrapper<Dict>().eq("value", value));

            if(null != dict) {
                return dict.getName();

            }

        } else {

            Dict parentDict = this.getDictByDictCode(dictCode);

            if(null == parentDict) return "";

            Dict dict = baseMapper.selectOne(new QueryWrapper<Dict>().eq("parent_id",

                    parentDict.getId()).eq("value", value));

            if(null != dict) {

                return dict.getName();

            }
        }

        return "";
    }

    @Override
    public List<Dict> findDictListByHoscode(String dictCode) {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("dict_code",dictCode);
        Dict dict = baseMapper.selectOne(queryWrapper);

        QueryWrapper<Dict> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("parent_id",dict.getId());
        List<Dict> dictList = baseMapper.selectList(queryWrapper2);
        return dictList;

    }

    private Dict getDictByDictCode(String dictCode) {

        QueryWrapper<Dict> wrapper = new QueryWrapper<>();

        wrapper.eq("dict_code",dictCode);

        Dict codeDict = baseMapper.selectOne(wrapper);

        return codeDict;
    }


}
