package com.swift.yygh.cmn.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.cmn.listener.MyApi2Listener;
import com.swift.yygh.cmn.mapper.MyApi2Mapper;
import com.swift.yygh.cmn.service.MyApi2Service;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.vo.cmn.DictEeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class MyApi2ServiceImpl extends ServiceImpl<MyApi2Mapper, Dict> implements MyApi2Service {
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public List<Dict> getDictList(Long parentId) {
        String key = "dict:category:"+parentId;
        Object values = redisTemplate.opsForValue().get(key);
        if(values != null){
            log.warn("从缓存中查询到数据{}",values);
            return (List<Dict>) values;
        }
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(!StringUtils.isEmpty(parentId),"parent_id",parentId);
        List<Dict> dictList = baseMapper.selectList(queryWrapper);
        dictList.forEach(dict -> {
            QueryWrapper<Dict> queryWrapper1 = new QueryWrapper<>();
            queryWrapper.eq(StringUtils.isEmpty(dict.getId()),"parent_id",dict.getId());
            Integer count = baseMapper.selectCount(queryWrapper);
            boolean result = count > 0;
            dict.setHasChildren(result);
        });

        redisTemplate.opsForValue().set(key,dictList);
        log.warn("从数据库中查询数据 {}",dictList);
        return dictList;
    }

    @Override
    public void upload(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), DictEeVo.class,new MyApi2Listener(baseMapper)).sheet().doRead();
    }

    @Override
    public void download(HttpServletResponse response) throws IOException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("数据字典", "UTF-8").replaceAll("\\+","%20");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        List<Dict> dictList = baseMapper.selectList(null);
        List<DictEeVo> dictEeVoList = new ArrayList<>();
        dictList.forEach(dict -> {
            DictEeVo dictEeVo = new DictEeVo();
            BeanUtils.copyProperties(dict,dictEeVo);
            dictEeVoList.add(dictEeVo);
        });
        EasyExcel.write(response.getOutputStream(), DictEeVo.class).sheet("数据字典").doWrite(dictEeVoList);
    }
}
