package com.swift.yygh.cmn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.cmn.Dict;

public interface MyApiMapper extends BaseMapper<Dict> {
}
