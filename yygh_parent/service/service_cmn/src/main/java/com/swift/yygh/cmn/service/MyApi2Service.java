package com.swift.yygh.cmn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.model.hosp.HospitalSet;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface MyApi2Service extends IService<Dict> {
    List<Dict> getDictList(Long parentId);

    void upload(MultipartFile file) throws IOException;

    void download(HttpServletResponse response) throws IOException;
}
