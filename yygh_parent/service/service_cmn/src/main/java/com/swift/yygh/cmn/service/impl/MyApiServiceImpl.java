package com.swift.yygh.cmn.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.cmn.listener.MyApiListener;
import com.swift.yygh.cmn.mapper.MyApiMapper;
import com.swift.yygh.cmn.service.MyApiService;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.vo.cmn.DictEeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Service
public class MyApiServiceImpl extends ServiceImpl<MyApiMapper, Dict> implements MyApiService {


    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<Dict> getDictListById(Long parentId) {

        //1先redis缓存中查询数据
        //1.1绑定一个String类型的key
        String key = "yygh:dict:parentId:"+parentId;
        //1.2根据key从redis中查询对应的value
        String dictListValue = (String)redisTemplate.opsForValue().get(key);
        //1.3判断以下value有没有数据，如果没有数据从数据库中查询数据并存入redis缓存中
        if(dictListValue == null){
            QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id",parentId);
            List<Dict> dictList = baseMapper.selectList(queryWrapper);
            for (Dict dict : dictList) {
                QueryWrapper<Dict> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq("parent_id",dict.getId());
                Integer count = baseMapper.selectCount(queryWrapper);
                boolean result = count > 0;
                dict.setHasChildren(result);
            }
            String s = JSON.toJSONString(dictList);
            //缓存到redis中
            redisTemplate.opsForValue().set(key,JSON.toJSONString(dictList));
            return dictList;
        }else {
            //说明redis缓存中有数据，可以直接从redis中获取数据
            List<Dict> list = JSON.parseObject(dictListValue, List.class);
            return list;
        }


    }

    @Override
    public void upload(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), DictEeVo.class,new MyApiListener(baseMapper)).sheet().doRead();
    }

    @Override
    public void download(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("数据字典", "UTF-8").replaceAll("\\+","%20");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        List<Dict> dictList = baseMapper.selectList(null);
        List<DictEeVo> dictEeVoList = new ArrayList<>();
        dictList.forEach(dict -> {
            DictEeVo dictEeVo = new DictEeVo();
            BeanUtils.copyProperties(dict,dictEeVo);
            dictEeVoList.add(dictEeVo);
        });
        EasyExcel.write(response.getOutputStream(), DictEeVo.class).sheet("模板").doWrite(dictEeVoList);
    }
}
