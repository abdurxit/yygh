package com.swift.yygh.cmn.controller;

import com.alibaba.fastjson.JSONObject;

import com.swift.yygh.cmn.utils.MinioUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
 
import javax.servlet.http.HttpServletRequest;
@Api(tags = "minio文件存储接口文档")
@Controller
@RequestMapping("/my")
public class MinioController {
 
    @Autowired
    private MinioUtils minioUtils;
 
 
    /**
     * 上传
     *
     * @param file
     * @param request
     * @return
     */
    @ApiOperation("文件上传接口")
    @PostMapping("/upload")
    @ResponseBody
    public String upload(@RequestParam(name = "file", required = false) MultipartFile file, HttpServletRequest request) {
        JSONObject res = null;
        try {
            res = minioUtils.uploadFile(file, "swift");
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code", 0);
            res.put("msg", "上传失败");
        }
        String str = res.toJSONString();
        System.out.println(str);
        return res.toJSONString();
    }
}
 