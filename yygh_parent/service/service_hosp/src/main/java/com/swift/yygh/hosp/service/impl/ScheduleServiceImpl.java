package com.swift.yygh.hosp.service.impl;


import com.swift.yygh.hosp.repostiry.DepartmentRepository;
import com.swift.yygh.hosp.repostiry.HospitalRepository;
import com.swift.yygh.hosp.repostiry.ScheduleRepository;
import com.swift.yygh.hosp.service.HospitalService;
import com.swift.yygh.hosp.service.ScheduleService;
import com.swift.yygh.model.hosp.BookingRule;
import com.swift.yygh.model.hosp.Department;
import com.swift.yygh.model.hosp.Hospital;
import com.swift.yygh.model.hosp.Schedule;
import com.swift.yygh.vo.hosp.BookingScheduleRuleVo;
import com.swift.yygh.vo.hosp.ScheduleOrderVo;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public void saveSchedule(Schedule schedule) {
       Schedule mongoSchedule= scheduleRepository.findByHoscodeAndDepcodeAndHosScheduleId(schedule.getHoscode(),schedule.getDepcode(),schedule.getHosScheduleId());
       if(mongoSchedule == null){
           schedule.setCreateTime(new Date());
           schedule.setUpdateTime(new Date());
           schedule.setIsDeleted(0);
           scheduleRepository.save(schedule);
       }else{
           schedule.setCreateTime(mongoSchedule.getCreateTime());
           schedule.setUpdateTime(new Date());
           schedule.setIsDeleted(mongoSchedule.getIsDeleted());
           schedule.setId(mongoSchedule.getId());
           scheduleRepository.save(schedule);
       }
    }

    @Override
    public Page<Schedule> page(Map<String, String> paramMap) {
        Schedule schedule = new Schedule();
        schedule.setHoscode(paramMap.get("hoscode"));
        schedule.setIsDeleted(0);
        Example<Schedule> example=Example.of(schedule);

        PageRequest pageRequest = PageRequest.of(Integer.parseInt(paramMap.get("page")) - 1, Integer.parseInt(paramMap.get("limit")));
        return  scheduleRepository.findAll(example, pageRequest);
    }

    @Override
    public Map<String, Object> getScheduleInfo(Long page, Long limit, String hoscode, String depcode) {
        //1 根据医院编号 和 科室编号 查询

        Criteria criteria = Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode);

        //2 根据工作日workDate期进行分组

        Aggregation agg = Aggregation.newAggregation(

                Aggregation.match(criteria),//匹配条件

                Aggregation.group("workDate")//分组字段

                        .first("workDate").as("workDate")

                //3 统计号源数量

                        .count().as("docCount")

                        .sum("reservedNumber").as("reservedNumber")

                        .sum("availableNumber").as("availableNumber"),

                //排序

                Aggregation.sort(Sort.Direction.ASC,"workDate"),

                //4 实现分页

                Aggregation.skip((page-1)*limit),

                Aggregation.limit(limit)

        );

        //调用方法，最终执行

        AggregationResults<BookingScheduleRuleVo> aggResults =

        mongoTemplate.aggregate(agg, Schedule.class, BookingScheduleRuleVo.class);

        List<BookingScheduleRuleVo> bookingScheduleRuleVoList = aggResults.getMappedResults();

        //分组查询的总记录数

        Aggregation totalAgg = Aggregation.newAggregation(

                Aggregation.match(criteria),

                Aggregation.group("workDate")

        );
        AggregationResults<BookingScheduleRuleVo> totalAggResults =

        mongoTemplate.aggregate(totalAgg,

                Schedule.class, BookingScheduleRuleVo.class);

        int total = totalAggResults.getMappedResults().size();

        //把日期对应星期获取

        for(BookingScheduleRuleVo bookingScheduleRuleVo:bookingScheduleRuleVoList) {
            Date workDate = bookingScheduleRuleVo.getWorkDate();
            String dayOfWeek = this.getDayOfWeek(new DateTime(workDate));
            bookingScheduleRuleVo.setDayOfWeek(dayOfWeek);
        }
        //设置最终数据，进行返回
        Map<String, Object> result = new HashMap<>();
        result.put("list",bookingScheduleRuleVoList);
        result.put("total",total);
        //获取医院名称

        Hospital hospital = hospitalService.getHospitalInfo(hoscode);

        //其他基础数据

        Map<String, String> baseMap = new HashMap<>();

        baseMap.put("hosname",hospital.getHosname());

        result.put("baseMap",baseMap);

        return result;
    }

    @Override
    public List<Schedule> getDetailSchedule(String hoscode, String depcode, String workDate) {
        List<Schedule> scheduleList =scheduleRepository.findByHoscodeAndDepcodeAndWorkDate(hoscode,depcode,new DateTime(workDate).toDate());
        return scheduleList;
    }

    @Override
    public Map<String, Object> selectpage(String hoscode, String depcode, Integer pageNum, Integer pageSize) {



        Hospital hospital = hospitalRepository.findByHoscode(hoscode);
        BookingRule bookingRule = hospital.getBookingRule();
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Date> page= getDatePage(pageNum,pageSize,bookingRule);
        List<Date> records = page.getRecords();


        Aggregation aggregation=Aggregation.newAggregation(
                Aggregation.match(Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode).and("workDate").in(records)),
                Aggregation.group("workDate")
                        .first("workDate").as("workDate")
                        .sum("reservedNumber").as("reservedNumber")
                        .sum("availableNumber").as("availableNumber"),
                Aggregation.sort(Sort.by(Sort.Order.asc("workDate")))
        );
        AggregationResults<BookingScheduleRuleVo> aggregate = mongoTemplate.aggregate(aggregation, Schedule.class, BookingScheduleRuleVo.class);
        List<BookingScheduleRuleVo> mappedResults = aggregate.getMappedResults();

        Map<Date, BookingScheduleRuleVo> collect = mappedResults.stream().collect(Collectors.toMap(BookingScheduleRuleVo::getWorkDate, BookingScheduleRuleVo -> BookingScheduleRuleVo));


        int length=records.size();

        List<BookingScheduleRuleVo> bookingScheduleRuleVoList=new ArrayList<BookingScheduleRuleVo>();

        for (int i = 0; i < length; i++) {
            Date date = records.get(i);
            BookingScheduleRuleVo bookingScheduleRuleVo = collect.get(date);
            if(bookingScheduleRuleVo== null){
                bookingScheduleRuleVo=new BookingScheduleRuleVo();
                bookingScheduleRuleVo.setWorkDate(date);
                bookingScheduleRuleVo.setReservedNumber(0);
                bookingScheduleRuleVo.setAvailableNumber(-1);
            }
            //
            bookingScheduleRuleVo.setDayOfWeek(getDayOfWeek(new DateTime(date)));
            bookingScheduleRuleVo.setStatus(0);

            //第一页第一条
            if(pageNum == 1 && i == 0){
                String stopTime = bookingRule.getStopTime();
                DateTime dateTime = convertDateAndStringToDateTime(date, stopTime);
                if(dateTime.isBeforeNow()){
                    bookingScheduleRuleVo.setStatus(-1);
                }
            }
            //最后一页的最后一条
            if(pageNum == page.getPages() && i == (length-1)){
                bookingScheduleRuleVo.setStatus(1);
            }
            bookingScheduleRuleVoList.add(bookingScheduleRuleVo);
        }

        Map<String,Object> resultMap=new HashMap<String,Object>();
        resultMap.put("list",bookingScheduleRuleVoList);
        resultMap.put("total",page.getTotal());

        //其他基础数据
        Map<String, String> baseMap = new HashMap<>();
        //医院名称
        baseMap.put("hosname", hospitalRepository.findByHoscode(hoscode).getHosname());
        //科室
        Department department =departmentRepository.findByHoscodeAndDepcode(hoscode, depcode);
        //大科室名称
        baseMap.put("bigname", department.getBigname());
        //科室名称
        baseMap.put("depname", department.getDepname());
        //月
        baseMap.put("workDateString", new DateTime().toString("yyyy年MM月"));
        //放号时间
        baseMap.put("releaseTime", bookingRule.getReleaseTime());
        //停号时间
        baseMap.put("stopTime", bookingRule.getStopTime());
        resultMap.put("baseMap",baseMap);
        return resultMap;
    }

    @Override
    public Schedule info(String id) {
        Schedule schedule = scheduleRepository.findById(id).get();
        String hoscode = schedule.getHoscode();
        String depcode = schedule.getDepcode();

        String hosname = hospitalRepository.findByHoscode(hoscode).getHosname();
        String departmentName = departmentRepository.findByHoscodeAndDepcode(hoscode, depcode).getDepname();
        schedule.getParam().put("hosname",hosname);
        schedule.getParam().put("depname",departmentName);
        schedule.getParam().put("dayOfWeek",  getDayOfWeek(new DateTime(schedule.getWorkDate())));
        return schedule;
    }

    @Override
    public ScheduleOrderVo remote(String id) {
        Schedule schedule = scheduleRepository.findById(id).get();
        ScheduleOrderVo scheduleOrderVo=new ScheduleOrderVo();

        BeanUtils.copyProperties(schedule,scheduleOrderVo);//源对象和目标对象的属性名要保持一致。
        String hoscode = schedule.getHoscode();
        String depcode = schedule.getDepcode();

        Hospital hospital = hospitalRepository.findByHoscode(hoscode);
        scheduleOrderVo.setHosname(hospital.getHosname());
        Department department = departmentRepository.findByHoscodeAndDepcode(hoscode, depcode);
        scheduleOrderVo.setDepname(department.getDepname());
        scheduleOrderVo.setReserveDate(schedule.getWorkDate());
        scheduleOrderVo.setReserveTime(schedule.getWorkTime());

        BookingRule bookingRule = hospital.getBookingRule();

        //退号截止时间
        DateTime dateTime = convertDateAndStringToDateTime(new DateTime(schedule.getWorkDate()).plusDays(bookingRule.getQuitDay()).toDate(), bookingRule.getQuitTime());
        scheduleOrderVo.setQuitTime(dateTime.toDate());

        //预约当天的挂号截止时间
        DateTime stopDateTime = convertDateAndStringToDateTime(schedule.getWorkDate(), bookingRule.getStopTime());
        scheduleOrderVo.setStopTime(stopDateTime.toDate());
        return scheduleOrderVo;
    }

    @Override
    public void updateAvailableNumber(String scheduleId, Integer availableNumber) {
        Optional<Schedule> optionalSchedule = scheduleRepository.findById(scheduleId);
        if (optionalSchedule.isPresent()){
            Schedule schedule = optionalSchedule.get();
            schedule.setAvailableNumber(availableNumber);
            scheduleRepository.save(schedule);
        }
    }

    @Override
    public void cacelSchedule(String scheduleId) {
        String[] split = scheduleId.split(":");
        Schedule schedule = scheduleRepository.findByHoscodeAndDepcodeAndHosScheduleId(split[0], split[1], split[2]);
        Integer availableNumber = schedule.getAvailableNumber();
        schedule.setAvailableNumber(availableNumber+1);
        scheduleRepository.save(schedule);
    }


    private com.baomidou.mybatisplus.extension.plugins.pagination.Page<Date> getDatePage(Integer pageNum, Integer pageSize, BookingRule bookingRule) {
        Integer cycle = bookingRule.getCycle();

        String releaseTime = bookingRule.getReleaseTime();
        DateTime dateTime= convertDateAndStringToDateTime(new Date(),releaseTime);
        if(dateTime.isBeforeNow()){
            cycle=cycle+1;
        }

        List<Date> dateList=new ArrayList<Date>();
        for(int i=0;i<cycle;i++){
            Date date = new DateTime(new DateTime().plusDays(i).toString("yyyy-MM-dd")).toDate();
            dateList.add(date);
        }

        int start=(pageNum-1)*pageSize;
        int end=start+pageSize;
        if(end>cycle){
            end=cycle;
        }

        List<Date> currentPageList=new ArrayList<Date>();
        for(int j=start;j<end;j++){
            currentPageList.add(dateList.get(j));
        }


      //  Page<Date> page=new Page<Date>(pageNum,pageSize,dateList.size());
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Date> page = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<Date>(pageNum,pageSize,dateList.size());
        page.setRecords(currentPageList);
        return page;

    }

    private DateTime convertDateAndStringToDateTime(Date date, String releaseTime) {
        String s = new DateTime(date).toString("yyyy-MM-dd") + " " + releaseTime;
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm").parseDateTime(s);
        return dateTime;
    }


    private String getDayOfWeek(DateTime dateTime) {

        String dayOfWeek = "";

        switch (dateTime.getDayOfWeek()) {

            case DateTimeConstants.SUNDAY:

                dayOfWeek = "周日";
                break;
            case DateTimeConstants.MONDAY:
                dayOfWeek = "周一";
                break;
            case DateTimeConstants.TUESDAY:
                dayOfWeek = "周二";
                break;

            case DateTimeConstants.WEDNESDAY:
                dayOfWeek = "周三";
                break;
            case DateTimeConstants.THURSDAY:
                dayOfWeek = "周四";
                break;
            case DateTimeConstants.FRIDAY:
                dayOfWeek = "周五";
                break;
            case DateTimeConstants.SATURDAY:
                dayOfWeek = "周六";
            default:
                break;
        }

        return dayOfWeek;

    }
}
