package com.swift.yygh.hosp.service;


import com.swift.yygh.model.hosp.Hospital;
import com.swift.yygh.vo.hosp.HospitalQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface HospitalService {
    void save(Map<String, String> paramMap);

    Hospital getHospitalInfo(String hoscode);

    Page<Hospital> getHospitalListByPage(Integer page, Integer limit, HospitalQueryVo hospitalQueryVo);

    void updateStatus(String id, Integer status);

    Hospital getDetail(String id);

    List<Hospital> findHospitalByHosnameLike(String hosname);

    Hospital findByHoscode(String hoscode);
}
