package com.swift.yygh.hosp.service;


import com.swift.yygh.model.hosp.Department;
import com.swift.yygh.model.hosp.Schedule;
import com.swift.yygh.vo.hosp.ScheduleOrderVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface ScheduleService {
    void saveSchedule(Schedule schedule);

    Page<Schedule> page(Map<String, String> paramMap);

    Map<String, Object> getScheduleInfo(Long page, Long limit, String hoscode, String depcode);

    List<Schedule> getDetailSchedule(String hoscode, String depcode, String workDate);

    Map<String, Object> selectpage(String hoscode, String depcode, Integer pageNum, Integer pageSize);

    Schedule info(String id);

    ScheduleOrderVo remote(String id);

    void updateAvailableNumber(String scheduleId, Integer availableNumber);

    void cacelSchedule(String scheduleId);
}
