package com.swift.yygh.hosp.repostiry;


import com.swift.yygh.model.hosp.Department;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends MongoRepository<Department,String> {

  public   Department findByHoscodeAndDepcode(String hoscode, String depcode);
}
