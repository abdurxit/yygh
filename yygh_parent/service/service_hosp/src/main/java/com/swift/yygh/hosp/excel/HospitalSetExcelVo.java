package com.swift.yygh.hosp.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;
import lombok.With;
import org.junit.jupiter.api.extension.ExtendWith;

@Data
public class HospitalSetExcelVo {
    @ExcelProperty(value = "医院设置id",index = 0)
    @ColumnWidth(20)
    private Long id;
    @ExcelProperty(value ="医院名称" ,index = 1)
    @ColumnWidth(20)
    private String hosname;
    @ExcelProperty(value = "医院编号",index = 2)
    @ColumnWidth(20)
    private String hoscode;
}
