package com.swift.yygh.hosp.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.hosp.excel.DictExcelVo;
import com.swift.yygh.hosp.listener.UploadDictListener;
import com.swift.yygh.hosp.mapper.MyDictTwoMapper;
import com.swift.yygh.hosp.service.MyDictTwoService;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.vo.cmn.DictEeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MyDictTwoServiceImpl extends ServiceImpl<MyDictTwoMapper, Dict> implements MyDictTwoService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<Dict> getDictList(Long parentId) {
        String key = "dict:info:"+parentId;
        Object value = redisTemplate.opsForValue().get(key);
        if(value != null){
            return (List<Dict>) value;
        }
        LambdaQueryWrapper<Dict> wrapper = Wrappers.lambdaQuery(Dict.class).eq(Dict::getParentId, parentId);
        List<Dict> dictList = baseMapper.selectList(wrapper);
        dictList.forEach(dict -> {
            LambdaQueryWrapper<Dict> wrapper1 = Wrappers.lambdaQuery(Dict.class).eq(Dict::getParentId, dict.getId());
            Integer count = baseMapper.selectCount(wrapper1);
            boolean result = count > 0 ;
            dict.setHasChildren(result);
        });
        redisTemplate.opsForValue().set(key,dictList);
        return dictList;
    }

    @Override
    public void downloadExcel(HttpServletResponse response) throws Exception {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("分类", "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        List<Dict> dictList = baseMapper.selectList(null);
        List<DictExcelVo> collect = dictList.stream().map(dict -> {
            DictExcelVo dictExcelVo = new DictExcelVo();
            BeanUtils.copyProperties(dict, dictExcelVo);
            return dictExcelVo;
        }).collect(Collectors.toList());
        EasyExcel.write(response.getOutputStream(), DictExcelVo.class).sheet("分类").doWrite(collect);
    }

    @Override
    public void uploadExcel(MultipartFile file) throws Exception{

        EasyExcel.read(file.getInputStream(), DictExcelVo.class, new UploadDictListener(baseMapper)).sheet().doRead();
    }
}
