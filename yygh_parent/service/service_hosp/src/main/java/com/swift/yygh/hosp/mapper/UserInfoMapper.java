package com.swift.yygh.hosp.mapper;

import com.swift.yygh.hosp.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author swift
 * @since 2023-03-09
 */
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
