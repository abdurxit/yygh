package com.swift.yygh.hosp.service;


import com.swift.yygh.model.hosp.Department;
import com.swift.yygh.vo.hosp.DepartmentVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface DepartmentService {
    void saveDepartment(Department department);

    Page<Department> page(Map<String, String> paramMap);

    void remove(Department department);

    List<DepartmentVo> getDepartmentListByHosCode(String hoscode);
}
