package com.swift.yygh.hosp.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.hosp.HospitalSet;

/**
 * <p>
 * 医院设置表 Mapper 接口
 * </p>
 *
 * @author swift
 * @since 2022-12-27
 */
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}
