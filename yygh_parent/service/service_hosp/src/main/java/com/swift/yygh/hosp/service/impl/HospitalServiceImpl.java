package com.swift.yygh.hosp.service.impl;

import com.alibaba.fastjson.JSONObject;

import com.swift.yygh.cmn.client.DictFeignClient;
import com.swift.yygh.enums.DictEnum;
import com.swift.yygh.hosp.repostiry.HospitalRepository;
import com.swift.yygh.hosp.service.HospitalService;
import com.swift.yygh.model.hosp.Hospital;
import com.swift.yygh.vo.hosp.HospitalQueryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class HospitalServiceImpl implements HospitalService {
    @Autowired
    private DictFeignClient dictFeignClient;
    @Autowired
   private HospitalRepository hospitalRepository;

    @Override
    public void save(Map<String, String> paramMap) {
        Hospital hospital = JSONObject.parseObject(JSONObject.toJSONString(paramMap), Hospital.class);

       Hospital mongoHospital= hospitalRepository.findByHoscode(hospital.getHoscode());
       if(mongoHospital==null){
           hospital.setStatus(0);//未上线
           hospital.setCreateTime(new Date());
           hospital.setUpdateTime(new Date());
           hospital.setIsDeleted(0);
           hospitalRepository.insert(hospital);
       }else{
           hospital.setStatus(mongoHospital.getStatus());//未上线
           hospital.setCreateTime(mongoHospital.getCreateTime());
           hospital.setUpdateTime(new Date());
           hospital.setIsDeleted(mongoHospital.getIsDeleted());

           hospital.setId(mongoHospital.getId());
           hospitalRepository.save(hospital);
       }


    }

    @Override
    public Hospital getHospitalInfo(String hoscode) {
       return hospitalRepository.findByHoscode(hoscode);
    }

    @Override
    public Page<Hospital> getHospitalListByPage(Integer page, Integer limit, HospitalQueryVo hospitalQueryVo) {

        Hospital hospital =new Hospital();
        BeanUtils.copyProperties(hospitalQueryVo,hospital);
        ExampleMatcher exampleMatcher = ExampleMatcher.matching().withMatcher("hosname", ExampleMatcher.GenericPropertyMatchers.contains());
        Example<Hospital> example=Example.of(hospital,exampleMatcher);

        PageRequest pageRequest = PageRequest.of(page - 1, limit, Sort.by(Sort.Order.desc("createTime")));
        Page<Hospital> pages = hospitalRepository.findAll(example, pageRequest);
        List<Hospital> content = pages.getContent();
        for (Hospital mongoHospital : content) {
            getHospitalInformation(mongoHospital);
        }
        return pages;
    }

    @Override
    public void updateStatus(String id, Integer status) {
        if(status == 0 || status == 1) {
            Hospital hospital = hospitalRepository.findById(id).get();
            hospital.setStatus(status);
            hospital.setUpdateTime(new Date());
            hospitalRepository.save(hospital);
        }
    }

    @Override
    public Hospital getDetail(String id) {
        Hospital hospital = hospitalRepository.findById(id).get();
        getHospitalInformation(hospital);
        return hospital;
    }

    @Override
    public List<Hospital> findHospitalByHosnameLike(String hosname) {
        return hospitalRepository.findHospitalByHosnameLike(hosname);

    }

    @Override
    public Hospital findByHoscode(String hoscode) {
        Hospital hospital = hospitalRepository.findByHoscode(hoscode);
        this.getHospitalInformation(hospital);
        return hospital;
    }

    private void getHospitalInformation(Hospital mongoHospital) {
        String provinceCode = mongoHospital.getProvinceCode();
        String cityCode = mongoHospital.getCityCode();
        String districtCode = mongoHospital.getDistrictCode();
        String hostype = mongoHospital.getHostype();

        String provinceName = dictFeignClient.getName(Long.parseLong(provinceCode));
        String cityName = dictFeignClient.getName(Long.parseLong(cityCode));
        String districtName = dictFeignClient.getName(Long.parseLong(districtCode));

        String hostypeString = dictFeignClient.getName(Long.parseLong(hostype), DictEnum.HOSTYPE.getDictCode());

        mongoHospital.getParam().put("fullAddress",provinceName+cityName+districtName+mongoHospital.getAddress());
        mongoHospital.getParam().put("hostypeString",hostypeString);


    }
}
