package com.swift.yygh.hosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.hosp.vo.LoginVo;
import com.swift.yygh.model.hosp.HospitalSet;
import com.swift.yygh.vo.user.RegisterVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface MyApiService extends IService<HospitalSet> {
    String upload(MultipartFile file);

    void downloadExcel(HttpServletResponse response);

    void uploadExcel(MultipartFile file);

    Boolean sendCode(String phone);

    String uploadVideo(MultipartFile file);


    String login(LoginVo loginVo);

    void register(RegisterVo registerVo);
}
