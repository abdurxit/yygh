package com.swift.yygh.hosp.controller.my;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.swift.yygh.common.result.R;
import com.swift.yygh.common.util.MD5;
import com.swift.yygh.hosp.service.MyApiService;
import com.swift.yygh.hosp.service.MyDictService;
import com.swift.yygh.hosp.vo.LoginVo;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.model.hosp.HospitalSet;
import com.swift.yygh.vo.hosp.HospitalSetQueryVo;
import com.swift.yygh.vo.user.RegisterVo;
import io.swagger.annotations.*;
import org.hibernate.validator.constraints.EAN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Api(tags = "我的编程接口")
@RestController
@RequestMapping("/my")
public class MyApiController {


    @Autowired
    private MyApiService myApiService;

    @Autowired
    private MyDictService myDictService;

  @ApiOperation("mybatisplus分页条件查询接口")
  @PostMapping("/selectByPage/{page}/{limit}")
  @ApiImplicitParams(
          value = {
                  @ApiImplicitParam(name = "page",value = "当前页",dataType = "integer",required = true,paramType = "path"),
                  @ApiImplicitParam(name = "limit",value = "每页显示条数",dataType = "integer",required = true,paramType = "path"),
                  @ApiImplicitParam(name = "hospitalSetQueryVo",value = "医院设置视图对象",required = false,paramType = "body")
          }
  )
  public R selectByPage(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit,
                        @RequestBody HospitalSetQueryVo hospitalSetQueryVo){


      Page<HospitalSet> hospitalSetPage = new Page<>(page, limit);

      QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
      //查询条件：根据医院名称模糊查询医院信息
      queryWrapper.like(!StringUtils.isEmpty(hospitalSetQueryVo.getHosname()),"hosname",hospitalSetQueryVo.getHosname());
      //查询条件：根据医院编号精确查询
      queryWrapper.eq(!StringUtils.isEmpty(hospitalSetQueryVo.getHoscode()),"hoscode",hospitalSetQueryVo.getHoscode());
      myApiService.page(hospitalSetPage,queryWrapper);
      return R.ok().data("total",hospitalSetPage.getTotal()).data("list",hospitalSetPage.getRecords());
  }

     @ApiOperation("添加操作接口")
     @PostMapping("/save")
     @ApiImplicitParam(name = "hospitalSet",value = "医院设置对象",dataType = "HospitalSet",required = true,paramType = "body")
     public R save(@RequestBody HospitalSet hospitalSet){


         String signKey = System.currentTimeMillis()+ new Random().nextInt(10000)+"";
         System.out.println(signKey);
         hospitalSet.setSignKey(MD5.encrypt(signKey));

         boolean save = myApiService.save(hospitalSet);
         if(save){
             return R.ok();
         }else {
             return R.error();
         }
     }

     @ApiOperation("根据id查询回显需要修改的信息接口")
     @GetMapping("/selectById/{id}")
     @ApiImplicitParam(name = "id",value = "主键id",required = true,dataType = "long",paramType = "path")
     public R selectById(@PathVariable("id") Long id){
         HospitalSet hospitalSet = myApiService.getById(id);
         return R.ok().data("item",hospitalSet);
     }


     @ApiOperation("修改操作接口")
     @PutMapping("/update")
     @ApiImplicitParam(name = "hospitalSet",value = "医院设置对象",required = true,dataType = "HospitalSet",paramType = "body")
     public R update(@RequestBody HospitalSet hospitalSet){

         boolean update = myApiService.saveOrUpdate(hospitalSet);
         if(update){
             return R.ok();
         }else {
             return R.error();
         }
     }

     @ApiOperation("根据Id删除接口")
     @DeleteMapping("/removeById/{id}")
     @ApiImplicitParam(name = "id",value = "主键id",required = true,dataType = "long",paramType = "path")
    public R deleteById(@PathVariable("id") Long id){

         boolean remove = myApiService.removeById(id);
         if(remove){
             return R.ok();
         }else {
             return R.error();
         }
     }

     @ApiOperation("批量删除接口")
     @DeleteMapping("/batchRemove")
     @ApiImplicitParam(name = "idList",value = "主键id列表",required = true,dataType = "array",paramType = "body")
     public R batchRemove(@RequestBody List<Long> idList){
         boolean remove = myApiService.removeByIds(idList);
         if(remove){
             return R.ok();
         }else {
             return R.error();
         }
     }

     @ApiOperation("修改状态接口")
     @PutMapping("/updateStatus/{id}/{status}")
     @ApiImplicitParams(
             value = {
                     @ApiImplicitParam(name = "id",value = "主键id",required = true,dataType = "long",paramType = "path"),
                     @ApiImplicitParam(name = "status",value = "医院锁定状态",required = true,dataType = "integer",paramType = "path")
             }
     )
     public R updateStatus(@PathVariable("id") Long id,@PathVariable("status") Integer status){

      if(status == 0 || status == 1){
          HospitalSet hospitalSet = myApiService.getById(id);
          hospitalSet.setStatus(status);
          hospitalSet.setUpdateTime(new Date());

          myApiService.saveOrUpdate(hospitalSet);

          return R.ok();
      }else {
          return R.error();
      }
     }

     @ApiOperation("文件上传接口")
     @PostMapping("/upload")
     @ApiParam(name = "file",value = "图片文件",required = true)
     public R upload(@RequestParam("file") MultipartFile file){
       String url =  myApiService.upload(file);
       return R.ok().data("url",url);
     }


     @ApiOperation("根据父节点id查询子节点数据接口")
     @GetMapping("/getDictListById/{parentId}")
     @ApiImplicitParam(name = "parentId",value = "父节点id",required = true,dataType = "long",paramType = "path")
     public R getDictList(@PathVariable("parentId") Long parentId){
        List<Dict> dictList =  myDictService.getDictList(parentId);
        return R.ok().data("list",dictList);
     }


     @ApiOperation("导出接口")
     @GetMapping("downloadExcel")
     public void downloadExcel(HttpServletResponse response){
      myApiService.downloadExcel(response);
     }


     @ApiOperation("导入接口")
     @PostMapping("uploadExcel")
     public R uploadExcel(@RequestParam("file") MultipartFile file){
      myApiService.uploadExcel(file);
      return R.ok();
     }

     @ApiOperation("发送短信接口")
    @GetMapping("send/{phone}")
    public R sendCode(@PathVariable("phone") String phone){
     Boolean result =  myApiService.sendCode(phone);
        if(result){
         return R.ok().message("发送短信成功");
        }else {
         return R.error().message("发送短信失败");
        }
     }


     @ApiOperation("上传视频接口")
     @PostMapping("uploadVod")
    public  R uploadVod(@RequestParam("file") MultipartFile file){
      //返回视频id
      String videoId =  myApiService.uploadVideo(file);
      return R.ok().data("videoId",videoId);

     }



     @ApiOperation("手机号短信登录接口")
     @PostMapping("login")
   // public R login(@RequestParam("phone") String phone,@RequestParam("code") String code){
    public R login(@RequestBody LoginVo loginVo){
        //返回token 字符串 使用jwt生成 token 按照一定的规则生成的。包含用户信息
       String token =   myApiService.login(loginVo);
       return R.ok().data("token",token);
     }



     @ApiOperation("手机号短信注册接口")
     @PostMapping("register")
    public R register(@RequestBody RegisterVo registerVo){
             myApiService.register(registerVo);
             return R.ok().message("注册成功");
     }





     
















}
