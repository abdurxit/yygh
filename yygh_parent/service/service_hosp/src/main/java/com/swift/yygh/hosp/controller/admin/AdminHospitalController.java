package com.swift.yygh.hosp.controller.admin;

import com.swift.yygh.common.result.R;
import com.swift.yygh.hosp.service.HospitalService;
import com.swift.yygh.hosp.service.HospitalSetService;
import com.swift.yygh.model.hosp.Hospital;
import com.swift.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@Api(tags = "医院管理接口")
@RestController
@RequestMapping("/admin/hosp")
//@CrossOrigin
public class AdminHospitalController {

    @Autowired
    private HospitalService hospitalService;
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page",value = "当前页数",required = true),
                    @ApiImplicitParam(name = "limit",value = "每页显示条数",required = true)
            }
    )
    @ApiOperation("分页条件查询医院管理接口")
    @PostMapping("/getHospitalListByPage/{page}/{limit}")
    public R getHospitalListByPage(@PathVariable("page") String page,@PathVariable("limit") String limit,
                                   @RequestBody HospitalQueryVo hospitalQueryVo){

       Page<Hospital> hospitalPage = hospitalService.getHospitalListByPage(Integer.parseInt(page),Integer.parseInt(limit),hospitalQueryVo);
       return R.ok().data("total",hospitalPage.getTotalElements()).data("list",hospitalPage.getContent());
    }


    @ApiOperation("修改上线状态接口")
    @PutMapping("/{id}/{status}")
    public R updateStatus(@PathVariable("id") String id ,@PathVariable("status") Integer status){
        hospitalService.updateStatus(id,status);
        return R.ok();
    }

    @ApiOperation("根据医院Id查询医院详情信息接口")
    @GetMapping("getDetailById/{id}")
    public R getDetailById(@PathVariable("id") String id){
        Hospital hospital = hospitalService.getDetail(id);
        return R.ok().data("hospital",hospital);
    }




}
