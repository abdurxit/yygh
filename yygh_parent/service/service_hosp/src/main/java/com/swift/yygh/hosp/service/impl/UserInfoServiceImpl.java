package com.swift.yygh.hosp.service.impl;

import com.swift.yygh.hosp.entity.UserInfo;
import com.swift.yygh.hosp.mapper.UserInfoMapper;
import com.swift.yygh.hosp.service.UserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author swift
 * @since 2023-03-09
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

}
