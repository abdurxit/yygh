package com.swift.yygh.hosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.cmn.Dict;

public interface MyDictMapper extends BaseMapper<Dict> {
}
