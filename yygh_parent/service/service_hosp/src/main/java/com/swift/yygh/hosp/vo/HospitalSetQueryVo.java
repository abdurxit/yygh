package com.swift.yygh.hosp.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("医院设置视图对象")
@Data
public class HospitalSetQueryVo {
    @ApiModelProperty(value = "医院编号")
    private String hoscode;
    @ApiModelProperty(value = "医院名称")
    private String hosname;
}
