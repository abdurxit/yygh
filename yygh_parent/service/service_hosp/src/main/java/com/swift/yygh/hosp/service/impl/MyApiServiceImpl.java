package com.swift.yygh.hosp.service.impl;

import com.alibaba.excel.EasyExcel;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.common.exception.MySwiftException;
import com.swift.yygh.common.util.MD5;
import com.swift.yygh.common.utils.JwtHelper;
import com.swift.yygh.hosp.config.VideoProperties;
import com.swift.yygh.hosp.entity.UserInfo;
import com.swift.yygh.hosp.excel.HospitalSetExcelVo;
import com.swift.yygh.hosp.listener.UploadHospListener;
import com.swift.yygh.hosp.mapper.MyApiMapper;
import com.swift.yygh.hosp.mapper.UserInfoMapper;
import com.swift.yygh.hosp.service.MyApiService;
import com.swift.yygh.hosp.service.UserInfoService;
import com.swift.yygh.hosp.utils.HttpUtils;
import com.swift.yygh.hosp.vo.LoginVo;
import com.swift.yygh.model.hosp.HospitalSet;
import com.swift.yygh.vo.user.RegisterVo;
import org.apache.http.HttpResponse;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@EnableConfigurationProperties(VideoProperties.class)
public class MyApiServiceImpl extends ServiceImpl<MyApiMapper,HospitalSet> implements MyApiService {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private VideoProperties videoProperties;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public String upload(MultipartFile file) {
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = "https://oss-cn-hangzhou.aliyuncs.com";
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = "LTAI5tNPgVZ1nDcDSAMtKp4R";
        String accessKeySecret = "aUjYMw2EeIwsyIhLWyNQZDR06IbRsn";
        // 填写Bucket名称，例如examplebucket。
        String bucketName = "swiftmall";
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
        String objectName = new DateTime().toString("yyyy/MM/dd/") + UUID.randomUUID().toString().replace("-", "") + file.getOriginalFilename();


        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            InputStream inputStream = file.getInputStream();
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, inputStream);
            // 设置该属性可以返回response。如果不设置，则返回的response为空。
            putObjectRequest.setProcess("true");
            // 创建PutObject请求。
            PutObjectResult result = ossClient.putObject(putObjectRequest);
            // 如果上传成功，则返回200。
        } catch (Exception oe) {
            oe.printStackTrace();
            return "出现异常了";
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return "https://" + bucketName + "." + endpoint + "/" + objectName;
    }

    @Override
    public void downloadExcel(HttpServletResponse response) {

        try {
            // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("医院设置", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            List<HospitalSet> hospitalSetList = baseMapper.selectList(null);
            List<HospitalSetExcelVo> hospitalSetExcelVos = new ArrayList<>();
            hospitalSetList.forEach(hospitalSet -> {
                HospitalSetExcelVo hospitalSetExcelVo = new HospitalSetExcelVo();
                BeanUtils.copyProperties(hospitalSet, hospitalSetExcelVo);
                hospitalSetExcelVos.add(hospitalSetExcelVo);
            });
            EasyExcel.write(response.getOutputStream(), HospitalSetExcelVo.class).sheet("医院设置数据").doWrite(hospitalSetExcelVos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadExcel(MultipartFile file) {
        try {
            EasyExcel.read(file.getInputStream(), HospitalSetExcelVo.class, new UploadHospListener(baseMapper)).sheet().doRead();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean sendCode(String phone) {

        Object values = redisTemplate.opsForValue().get(phone);
        if (!StringUtils.isEmpty(values)) {
            return true;
        }
        String host = "http://dingxin.market.alicloudapi.com";
        String path = "/dx/sendSms";
        String method = "POST";

        String appcode = "812b2a827a004fe0ba8e680f630741ca";

        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("mobile", phone);
        // String fourBitRandom = RandomUtil.getFourBitRandom();
        String code = new Random().nextInt(1000) + "";
        querys.put("param", "code:" + code);
        querys.put("tpl_id", "TP1711063");
        Map<String, String> bodys = new HashMap<String, String>();


        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.DAYS);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String uploadVideo(MultipartFile file) {
        try {
            //上传文件的原始名称
            String fileName = file.getOriginalFilename();
            //视频名称
            String title = fileName.substring(0, fileName.lastIndexOf("."));
            InputStream inputStream = file.getInputStream();
            UploadStreamRequest request = new UploadStreamRequest(videoProperties.getAccessKeyId(), videoProperties.getAccessKeySecret(), title, fileName, inputStream);
            UploadVideoImpl uploader = new UploadVideoImpl();
            UploadStreamResponse response = uploader.uploadStream(request);
            String videoId = null;

            if (response.isSuccess()) {
                System.out.print("VideoId=" + response.getVideoId() + "\n");
                videoId = response.getVideoId();

            } else { //如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因
                videoId = response.getVideoId();
            }
            return videoId;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String login(LoginVo loginVo) {
        //获取请求参数
        String phone = loginVo.getPhone();
        String password = loginVo.getPassword();
        String code = loginVo.getCode();
        if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)) {

            throw new MySwiftException(20001,"手机号和密码不能为空");
        }
        //判断这个手机号是否存在
        UserInfo userInfo = userInfoService.getOne(Wrappers.lambdaQuery(UserInfo.class).eq(UserInfo::getMobile, phone));
        if(userInfo ==null){
            throw new MySwiftException(20001,"这个用户不存在，请注册");
        }

        //判断用户输入的密码
        if(!MD5.encrypt(password).equals(userInfo.getPassword())){
            throw new MySwiftException(20001,"密码错误");
        }

        //判断用户是否被禁用
        if(userInfo.getIsDisabled().intValue() == 1){
            throw new MySwiftException(20001,"该用户已禁用");
        }

        //判断验证码
        Object value = redisTemplate.opsForValue().get(phone);
        if(StringUtils.isEmpty(code) || !code.equals(value)){
            throw new MySwiftException(20001,"验证码错误");
        }

        //登录成功生成token字符串
        String token = JwtHelper.createToken(userInfo.getId(), userInfo.getNickname());
        return token;
    }

    @Override
    public void register(RegisterVo registerVo) {
        String mobile = registerVo.getMobile();
        String code = registerVo.getCode();
        String password = registerVo.getPassword();

        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(code) || StringUtils.isEmpty(password)){
            throw new MySwiftException(20001,"注册失败");
        }

        Integer count = userInfoMapper.selectCount(Wrappers.lambdaQuery(UserInfo.class).eq(UserInfo::getMobile, mobile));
        if(count > 0){
            throw new MySwiftException(20001,"注册失败");
        }

        Object redisCode = redisTemplate.opsForValue().get(mobile);
        if(!redisCode.equals(code)){
            throw new MySwiftException(20001,"注册失败");
        }


        UserInfo userInfo = new UserInfo();
        userInfo.setMobile(mobile);
        userInfo.setPassword(MD5.encrypt(password));
        userInfo.setNickname(new Random().nextInt(100000)+"");
        userInfo.setIsDisabled(0);
        userInfo.setAvatar("https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIyFavZ2yJOZE1KdFibyhE1S4j6vmeNeib5QwYckFNm1txWqKctKWvnLic3QcXcKZSKqTySLKhXK1GIg/132");
        userInfoMapper.insert(userInfo);

    }
}