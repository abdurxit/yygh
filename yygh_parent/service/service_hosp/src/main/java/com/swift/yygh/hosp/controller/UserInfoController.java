package com.swift.yygh.hosp.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author swift
 * @since 2023-03-09
 */
@RestController
@RequestMapping("/hosp/user-info")
public class UserInfoController {

}

