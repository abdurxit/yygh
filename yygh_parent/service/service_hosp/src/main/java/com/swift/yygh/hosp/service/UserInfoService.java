package com.swift.yygh.hosp.service;

import com.swift.yygh.hosp.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author swift
 * @since 2023-03-09
 */
public interface UserInfoService extends IService<UserInfo> {

}
