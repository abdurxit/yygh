package com.swift.yygh.hosp.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.swift.yygh.hosp.excel.DictExcelVo;
import com.swift.yygh.hosp.mapper.MyDictTwoMapper;
import com.swift.yygh.model.cmn.Dict;
import org.springframework.beans.BeanUtils;

public class UploadDictListener extends AnalysisEventListener<DictExcelVo> {

    private MyDictTwoMapper myDictTwoMapper;

    public UploadDictListener(MyDictTwoMapper myDictTwoMapper) {
        this.myDictTwoMapper = myDictTwoMapper;
    }

    @Override
    public void invoke(DictExcelVo data, AnalysisContext context) {
        Dict dict = new Dict();
        BeanUtils.copyProperties(data,dict);
        Dict dict1 = myDictTwoMapper.selectById(dict.getId());
        if(dict1!=null){
            myDictTwoMapper.insert(dict1);
        }else {
            myDictTwoMapper.updateById(dict1);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
