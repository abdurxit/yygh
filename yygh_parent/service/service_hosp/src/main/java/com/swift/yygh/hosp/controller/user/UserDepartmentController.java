package com.swift.yygh.hosp.controller.user;

import com.swift.yygh.common.result.R;
import com.swift.yygh.hosp.service.DepartmentService;
import com.swift.yygh.vo.hosp.DepartmentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/user/department")
public class UserDepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/list/{hoscode}")
    public R getDepartmentListByHoscode(@PathVariable String hoscode){
        List<DepartmentVo> bigDepartmentList= departmentService.getDepartmentListByHosCode(hoscode);
        return R.ok().data("list",bigDepartmentList);
    }

}

