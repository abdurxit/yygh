package com.swift.yygh.hosp.service.impl;

import com.swift.yygh.hosp.repostiry.DepartmentRepository;
import com.swift.yygh.hosp.service.DepartmentService;
import com.swift.yygh.model.hosp.Department;
import com.swift.yygh.vo.hosp.DepartmentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public void saveDepartment(Department department) {

       Department mongoDepartment=  departmentRepository.findByHoscodeAndDepcode(department.getHoscode(),department.getDepcode());
       if(mongoDepartment== null){
           department.setCreateTime(new Date());
           department.setUpdateTime(new Date());
           department.setIsDeleted(0);
           departmentRepository.save(department);
       }else{
           department.setCreateTime(mongoDepartment.getCreateTime());
           department.setUpdateTime(new Date());
           department.setIsDeleted(mongoDepartment.getIsDeleted());
           department.setId(mongoDepartment.getId());

           departmentRepository.save(department);
       }
    }

    @Override
    public Page<Department> page(Map<String, String> paramMap) {


        Department department=new Department();
        department.setHoscode(paramMap.get("hoscode"));
        department.setIsDeleted(0);
        Example<Department> example=Example.of(department);

        PageRequest pageRequest = PageRequest.of(Integer.parseInt(paramMap.get("page")) - 1, Integer.parseInt(paramMap.get("limit")));
        return  departmentRepository.findAll(example, pageRequest);
    }

    @Override
    public void remove(Department department) {
        Department mongoDepartment = departmentRepository.findByHoscodeAndDepcode(department.getHoscode(), department.getDepcode());
        if(mongoDepartment != null){
            mongoDepartment.setIsDeleted(1);
            mongoDepartment.setUpdateTime(new Date());
            departmentRepository.save(mongoDepartment);
        }

    }

    @Override
    public List<DepartmentVo> getDepartmentListByHosCode(String hoscode) {
        //创建list集合，用于最终数据封装
        List<DepartmentVo> result = new ArrayList<>();

        //根据医院编号，查询医院所有科室信息
        Department departmentQuery = new Department();
        departmentQuery.setHoscode(hoscode);
        Example example = Example.of(departmentQuery);
        //所有科室列表 departmentList
        List<Department> departmentList = departmentRepository.findAll(example);

        //根据大科室编号  bigcode 分组，获取每个大科室里面下级子科室
        Map<String, List<Department>> deparmentMap =
                departmentList.stream().collect(Collectors.groupingBy(Department::getBigcode));
        //遍历map集合 deparmentMap
        for(Map.Entry<String,List<Department>> entry : deparmentMap.entrySet()) {
            //大科室编号
            String bigcode = entry.getKey();
            //大科室编号对应的全局数据
            List<Department> deparment1List = entry.getValue();
            //封装大科室
            DepartmentVo departmentVo1 = new DepartmentVo();
            departmentVo1.setDepcode(bigcode);
            departmentVo1.setDepname(deparment1List.get(0).getBigname());

            //封装小科室
            List<DepartmentVo> children = new ArrayList<>();
            for(Department department: deparment1List) {
                DepartmentVo departmentVo2 =  new DepartmentVo();
                departmentVo2.setDepcode(department.getDepcode());
                departmentVo2.setDepname(department.getDepname());
                //封装到list集合
                children.add(departmentVo2);
            }
            //把小科室list集合放到大科室children里面
            departmentVo1.setChildren(children);
            //放到最终result里面
            result.add(departmentVo1);
        }
        //返回
        return result;
    }
}


