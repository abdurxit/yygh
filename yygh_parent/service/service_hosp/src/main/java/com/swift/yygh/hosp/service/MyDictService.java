package com.swift.yygh.hosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.cmn.Dict;

import java.util.List;

public interface MyDictService extends IService<Dict> {
    List<Dict> getDictList(Long parentId);
}
