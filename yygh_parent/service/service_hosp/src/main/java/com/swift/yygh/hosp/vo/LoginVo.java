package com.swift.yygh.hosp.vo;

import lombok.Data;

@Data
public class LoginVo {
    private String phone;
    private String code;
    private String password;
}
