package com.swift.yygh.hosp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ali")
@Data
public class VideoProperties {
    private String accessKeyId;
     private String accessKeySecret;
}
