package com.swift.yygh.hosp.controller.admin;

import com.swift.yygh.common.result.R;
import com.swift.yygh.hosp.service.ScheduleService;
import com.swift.yygh.model.hosp.Schedule;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags = "排班信息接口")
@RestController
@RequestMapping("/admin/schedule")
//@CrossOrigin
public class AdminScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @GetMapping("/getScheduleInfo/{page}/{limit}/{hoscode}/{depcode}")
    public R getScheduleInfo(@PathVariable("page") Long page, @PathVariable("limit") Long limit,
                             @PathVariable("hoscode") String hoscode, @PathVariable("depcode") String depcode){

        Map<String,Object> map =  scheduleService.getScheduleInfo(page,limit,hoscode,depcode);
        return R.ok().data(map);
    }

    @GetMapping("getScheduleDetail/{hoscode}/{depcode}/{workDate}")
    public R getScheduleDetail(@PathVariable("hoscode") String hoscode,@PathVariable("depcode") String depcode,
                               @PathVariable("workDate") String workDate){

        List<Schedule> list = scheduleService.getDetailSchedule(hoscode,depcode,workDate);
        return R.ok().data("list",list);
    }
}
