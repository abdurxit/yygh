package com.swift.yygh.hosp.listener;


import com.rabbitmq.client.Channel;
import com.swift.yygh.hosp.service.ScheduleService;
import com.swift.yygh.mq.constant.MqConst;
import com.swift.yygh.vo.msm.MsmVo;
import com.swift.yygh.vo.order.OrderMqVo;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderListener {

    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(bindings={
            @QueueBinding(
                    exchange=@Exchange(name = MqConst.EXCHANGE_ORDER,type = "direct"),
                    value=@Queue(name = MqConst.QUEUE_ORDER),
                    key = MqConst.ROUTING_KEY_ORDER
            )
    })
    public void consume(OrderMqVo orderMqVo, Message message, Channel channel){
        String scheduleId = orderMqVo.getScheduleId();
        Integer availableNumber = orderMqVo.getAvailableNumber();
        if(availableNumber != null){
            scheduleService.updateAvailableNumber(scheduleId,availableNumber);
        }else{
            scheduleService.cacelSchedule(scheduleId);
        }
        MsmVo msmVo = orderMqVo.getMsmVo();
        if(msmVo != null){
            rabbitTemplate.convertAndSend(MqConst.EXCHANGE_SMS,MqConst.ROUTING_KEY_SMS,msmVo);
        }
    }
}
