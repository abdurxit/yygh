package com.swift.yygh.hosp.controller.user;

import com.swift.yygh.common.result.R;
import com.swift.yygh.hosp.service.HospitalService;
import com.swift.yygh.model.hosp.Hospital;
import com.swift.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(tags = "用户管理系统医院管理接口")
@RestController
@RequestMapping("/user/hospital")
public class UserHospitalController {


    @Autowired
    private HospitalService hospitalService;


    @ApiOperation("分页条件查询接口")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public R page(@PathVariable Integer pageNum,
                  @PathVariable Integer pageSize,
                  @RequestBody HospitalQueryVo hospitalQueryVo){
        Page<Hospital> page = hospitalService.getHospitalListByPage(pageNum, pageSize, hospitalQueryVo);

        return R.ok().data("list",page.getContent());
    }

    @ApiOperation("根据医院名称查询医院接口")
    @PostMapping("/findByHosname/{hosname}")
    public R findByHosname(@PathVariable String  hosname){
        List<Hospital> hospitalList = hospitalService.findHospitalByHosnameLike(hosname);
        return R.ok().data("list",hospitalList);
    }

    @GetMapping("/findByHoscode/{hoscode}")
    public R findByHoscode(@PathVariable String  hoscode){
        Hospital hospital= hospitalService.findByHoscode(hoscode);

        return R.ok().data("hospital",hospital);
    }
}