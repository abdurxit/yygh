package com.swift.yygh.hosp.controller.api;


import com.swift.yygh.common.result.Result;
import com.swift.yygh.hosp.service.DepartmentService;
import com.swift.yygh.model.hosp.Department;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(tags = "医院科室管理接口")
@RestController
@RequestMapping("/api/hosp")
public class ApiDepartmentController {


    @Autowired
    private DepartmentService departmentService;

    @ApiOperation("保存科室信息接口")
    @PostMapping("/saveDepartment")
    public Result saveDepartment(Department department){
      //1.进行signKey校验
        departmentService.saveDepartment(department);
        return Result.ok();
    }

    @ApiOperation("分页查询科室信息接口")
    @PostMapping("/department/list")
    public  Result<Page> page(@RequestParam Map<String,String> paramMap){
      //1.进行signKey校验
       Page<Department> page= departmentService.page(paramMap);
       return Result.ok(page);
    }

    @PostMapping("/department/remove")
    public Result remove(Department department){
        //1.进行signKey校验
        departmentService.remove(department);
        return Result.ok();
    }
}
