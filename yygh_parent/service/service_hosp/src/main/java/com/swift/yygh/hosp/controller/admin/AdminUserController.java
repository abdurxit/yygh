package com.swift.yygh.hosp.controller.admin;

import com.swift.yygh.common.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
@Api(tags = "管理系统用户接口文档")
@RestController
@RequestMapping("/admin/user")
//@CrossOrigin
public class AdminUserController {

    @ApiOperation("管理员登录接口")
    @PostMapping("/login")
    public R login(@RequestBody Map<String,Object> map){
        map.put("admin","admin-token");
        return R.ok().data(map);
    }

    @ApiOperation("获取管理员信息接口")
    @GetMapping("/info")
    public R getInfo(String user){
        Map<String,Object> map = new HashMap<>();
        map.put("roles","[admin]");
        map.put("introduction","I am a super administrator");
        map.put("avatar","https://img.soogif.com/3Y6lnVsxbGuhXTSvbndBGRXx40DeIic6.gif");
        map.put("name","Super abdurxit");
        return R.ok().data(map);
    }

    @ApiOperation("退出登录接口")
    @PostMapping("/logout")
    public R logout(){
        return R.ok();
    }
}
