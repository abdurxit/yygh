package com.swift.yygh.hosp.controller.admin;

import com.swift.yygh.common.result.R;
import com.swift.yygh.hosp.service.DepartmentService;
import com.swift.yygh.hosp.service.HospitalService;
import com.swift.yygh.model.hosp.Hospital;
import com.swift.yygh.vo.hosp.DepartmentVo;
import com.swift.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(tags = "部门信息接口")
@RestController
@RequestMapping("/admin/department")
//@CrossOrigin
public class AdminDepartmentController {

    @Autowired
    private DepartmentService departmentService;



    @GetMapping("/getDepartmentListByHosCode/{hoscode}")
    public R getDepartmentListByHosCode(@PathVariable("hoscode") String hoscode){
      List<DepartmentVo> departmentList = departmentService.getDepartmentListByHosCode(hoscode);
      return R.ok().data("list",departmentList);
    }





}
