package com.swift.yygh.hosp.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.swift.yygh.common.exception.MyYyghException;
import com.swift.yygh.common.util.MD5;
import com.swift.yygh.hosp.mapper.HospitalSetMapper;
import com.swift.yygh.hosp.service.HospitalSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.model.hosp.HospitalSet;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医院设置表 服务实现类
 * </p>
 *
 * @author swift
 * @since 2022-12-27
 */
@Service
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet> implements HospitalSetService {

    @Override
    public String getPlatformSignKey(String hoscode) {
        QueryWrapper<HospitalSet> queryWrapper=new QueryWrapper<HospitalSet>();
        queryWrapper.eq("hoscode",hoscode);
        HospitalSet hospitalSet = baseMapper.selectOne(queryWrapper);
        if(hospitalSet == null){
            throw new MyYyghException(2001,"此医院信息不存在");
        }
        return MD5.encrypt(hospitalSet.getSignKey());
    }
}
