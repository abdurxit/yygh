package com.swift.yygh.hosp.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.swift.yygh.common.exception.MyYyghException;
import com.swift.yygh.common.result.R;
import com.swift.yygh.common.util.MD5;
import com.swift.yygh.hosp.service.HospitalSetService;
import com.swift.yygh.model.hosp.HospitalSet;
import com.swift.yygh.vo.hosp.HospitalSetQueryVo;
import io.swagger.annotations.*;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * <p>
 * 医院设置表 前端控制器
 * </p>
 *
 * @author swift
 * @since 2022-12-27
 */
//@CrossOrigin
@RestController
@RequestMapping("/admin/hospset/hospitalSet")
@Api(tags = "医院设置接口")
public class AdminHospitalSetController {

    @Autowired
    private HospitalSetService hospitalSetService;

   /* @ApiOperation(value = "分页查询所有医院设置接口")
    @GetMapping("/findByPage/{page}/{limit}")
    public R findByPage(@ApiParam(name = "page",value = "当前页",required = true,defaultValue = "1") @PathVariable("page") Long page,
                        @ApiParam(name = "limit",value = "每页显示条数",required = true,defaultValue = "3") @PathVariable("limit")Long limit){
        Page<HospitalSet> hospitalSetPage = new Page<>(page,limit);
        Page<HospitalSet> setPage = hospitalSetService.page(hospitalSetPage);
        List<OrderItem> rows = setPage.getOrders();
        long total = setPage.getTotal();
        return R.ok().data("total",total).data("rows",rows);
    }*/

    @ApiResponses({
            @ApiResponse(code = 20000,message = "成功",response =R.class ),
            @ApiResponse(code = 20001,message = "失败",response =R.class )
    })
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page",value = "当前页",required = true,defaultValue = "1",dataType = "Long",paramType = "path"),
                    @ApiImplicitParam(name = "limit",value = "每页显示条数",required = true,defaultValue = "3",dataType = "Long",paramType = "path")
            }
    )
    @ApiOperation(value = "分页查询医院设置接口")
    @GetMapping("/findByPage/{page}/{limit}")
    public R findByPage(@PathVariable("page") Long page,@PathVariable("limit") Long limit){
        Page<HospitalSet> hospitalSetPage = new Page<>(page,limit);
        Page<HospitalSet> setPage = hospitalSetService.page(hospitalSetPage);
        List<HospitalSet> rows = setPage.getRecords();
        long total = setPage.getTotal();
        System.out.println(total+"==================>");

        return R.ok().data("total",hospitalSetPage.getTotal()).data("rows",rows);
    }

    @ApiOperation(value = "查询所有医院设置接口")
    @GetMapping("/findAll")
    public R findAll(){

        List<HospitalSet> list = hospitalSetService.list();
      /*  try {
            int i = 1/0;
        }catch (Exception e){
            throw new MyYyghException(20001,"医院设置查询1/0异常");
        }*/
        return R.ok().data("list",list);
    }



    @ApiOperation(value = "新增医院设置接口")
    @PostMapping("/save")
    public R save(@ApiParam(name = "hospitalSet",value = "医院设置对象",required = true)
                      @RequestBody HospitalSet hospitalSet){
        //设置可用状态 0 可用  1 不可用
        hospitalSet.setStatus(1);
        //签名秘钥
        String signKey = System.currentTimeMillis()+new Random().nextInt(10000)+"";
        hospitalSet.setSignKey(MD5.encrypt(signKey));
        boolean result = hospitalSetService.save(hospitalSet);
        if(result){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation("根据ID查询医院设置接口")
    @GetMapping("/edit/{id}")
    public R edit(
            @PathVariable("id") String id){
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        return R.ok().data("item",hospitalSet);
    }

    @ApiOperation("修改医院设置接口")
    @PutMapping("/update")
    public R update(@ApiParam(name = "hospitalSet",value = "医院设置对象",required = true)
                        @RequestBody HospitalSet hospitalSet){
        boolean result = hospitalSetService.updateById(hospitalSet);
        if(result){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation("删除医院设置接口")
    @DeleteMapping("/remove/{id}")
    public R remove(@ApiParam(name = "id",value = "医院设置Id",required = true)
                    @PathVariable("id") String id){
        boolean result = hospitalSetService.removeById(id);
        if(result){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation("批量删除医院设置接口")
    @DeleteMapping("/batchRemove")
    public R batchRemove(@ApiParam(name = "idList" ,value = "医院设置Id列表",required = true)
            @RequestBody List<Long> idList){
        boolean result = hospitalSetService.removeByIds(idList);
        if(result){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation("锁定和解锁医院设置接口")
    @PutMapping("lockHospitalSet/{id}/{status}")
    @ApiImplicitParams(
            value = {@ApiImplicitParam(name = "id",value = "医院设置Id",required = true),
                    @ApiImplicitParam(name = "status",value = "医院锁定状态",required = true)}
    )
    public R lockHospitalSet(@PathVariable("id") String id, @PathVariable("status") Integer status){
        //根据Id查询医院设置
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        //设置状态
        hospitalSet.setStatus(status);
        //修改医院设置
        boolean result = hospitalSetService.updateById(hospitalSet);
        if(result){
            return R.ok();
        }else {
            return R.error();
        }
    }



    @ApiOperation("发送签名和密钥接口")
    @PutMapping("/sendKey/{id}")
    @ApiImplicitParam(name = "id",value = "医院设置接口",required = true)
    public R sendKey(@PathVariable("id") String  id){
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        String signKey = hospitalSet.getSignKey();
        String hoscode = hospitalSet.getHoscode();
        //TODO 发送短信
        return  R.ok();
    }

    @ApiOperation("分页条件查询医院设置接口")
    @PostMapping("/{page}/{limit}")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "page",value = "当前页",required = true),
            @ApiImplicitParam(name = "limit",value = "每页显示条数",required = true),
            @ApiImplicitParam(name = "hospitalSetQueryVo",value = "查询条件对象",required = false)
    })
    public R PageQuery(@PathVariable("page") Long page, @PathVariable("limit") Long limit,
                       @RequestBody HospitalSetQueryVo hospitalSetQueryVo){

        Page<HospitalSet> setPage = new Page<>(page,limit);
        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(!StringUtils.isEmpty(hospitalSetQueryVo.getHosname()),"hosname",hospitalSetQueryVo.getHosname());
        queryWrapper.eq(!StringUtils.isEmpty(hospitalSetQueryVo.getHoscode()),"hoscode",hospitalSetQueryVo.getHoscode());
         hospitalSetService.page(setPage, queryWrapper);
        List<HospitalSet> rows = setPage.getRecords();
        return R.ok().data("total",setPage.getTotal()).data("rows",rows);
    }










}

