package com.swift.yygh.hosp.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;
import com.swift.yygh.common.exception.MySwiftException;
import com.swift.yygh.common.util.MD5;
import com.swift.yygh.common.utils.JwtHelper;
import com.swift.yygh.hosp.config.VideoProperties;
import com.swift.yygh.hosp.entity.UserInfo;
import com.swift.yygh.hosp.mapper.MyApiTwoMapper;
import com.swift.yygh.hosp.service.MyApiTwoService;
import com.swift.yygh.hosp.service.UserInfoService;
import com.swift.yygh.hosp.utils.HttpUtils;
import com.swift.yygh.hosp.vo.LoginVo;
import com.swift.yygh.model.hosp.HospitalSet;
import org.apache.http.HttpResponse;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@EnableConfigurationProperties(VideoProperties.class)
public class MyApiTwoMapperImpl extends ServiceImpl<MyApiTwoMapper, HospitalSet> implements MyApiTwoService {
   @Autowired
   private VideoProperties videoProperties;

   @Autowired
   private RedisTemplate redisTemplate;

   @Autowired
   private UserInfoService userInfoService;

    @Override
    public String upload(MultipartFile file) {

        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = "oss-cn-hangzhou.aliyuncs.com";
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = "LTAI5tNPgVZ1nDcDSAMtKp4R";
        String accessKeySecret = "aUjYMw2EeIwsyIhLWyNQZDR06IbRsn";
        // 填写Bucket名称，例如examplebucket。
        String bucketName = "swiftmall";
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
        String objectName = new DateTime().toString("yyyy/MM/dd/")+ UUID.randomUUID().toString().replace("-","")+file.getOriginalFilename();


        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            InputStream inputStream = file.getInputStream();
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, inputStream);
            // 设置该属性可以返回response。如果不设置，则返回的response为空。
            putObjectRequest.setProcess("true");
            // 创建PutObject请求。
            PutObjectResult result = ossClient.putObject(putObjectRequest);
            // 如果上传成功，则返回200。
            System.out.println(result.getResponse().getStatusCode());
        } catch (Exception oe) {
           return "";
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }

        String url = "https://"+bucketName+"."+endpoint+"/"+objectName;

        return url;
    }

    @Override
    public String uploadVideo(MultipartFile file)  {
        try {
            String fileName = file.getOriginalFilename();
            String title = fileName.substring(0,fileName.lastIndexOf("."));
            InputStream inputStream = file.getInputStream();

            UploadStreamRequest request = new UploadStreamRequest(videoProperties.getAccessKeyId(), videoProperties.getAccessKeySecret(), title, fileName, inputStream);

            UploadVideoImpl uploader = new UploadVideoImpl();
            UploadStreamResponse response = uploader.uploadStream(request);
            System.out.print("RequestId=" + response.getRequestId() + "\n");  //请求视频点播服务的请求ID
            String videoId = null;
            if (response.isSuccess()) {
                System.out.print("VideoId=" + response.getVideoId() + "\n");
               videoId = response.getVideoId();
            } else { //如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因
               videoId = response.getVideoId();
            }
            return videoId;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Boolean sendCode(String phone) {

        Object values = redisTemplate.opsForValue().get(phone);
        if (!StringUtils.isEmpty(values)) {
            return true;
        }
        String host = "http://dingxin.market.alicloudapi.com";
        String path = "/dx/sendSms";
        String method = "POST";

        String appcode = "812b2a827a004fe0ba8e680f630741ca";

        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("mobile", phone);
        // String fourBitRandom = RandomUtil.getFourBitRandom();
        String code = new Random().nextInt(10000) + "";
        querys.put("param", "code:" + code);
        querys.put("tpl_id", "TP1711063");
        Map<String, String> bodys = new HashMap<String, String>();


        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.DAYS);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String login(LoginVo loginVo) {
        String phone = loginVo.getPhone();
        String code = loginVo.getCode();
        String password = loginVo.getPassword();

        if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(code) || StringUtils.isEmpty(password)){
            throw new MySwiftException(20001,"登录失败");
        }

        //判断手机号是否存在
        UserInfo userInfo = userInfoService.getOne(Wrappers.lambdaQuery(UserInfo.class).eq(UserInfo::getMobile, phone));
        if(StringUtils.isEmpty(userInfo)){
            throw new MySwiftException(20001,"登录失败");
        }

        //判断验证码是否正确
        Object redisCode = redisTemplate.opsForValue().get(phone);
        if(!redisCode.equals(code)){
            throw new MySwiftException(20001,"登录失败");
        }

        //判断密码是否正确
        String infoPassword = userInfo.getPassword();
        if(!MD5.encrypt(password).equals(infoPassword)){
            throw new MySwiftException(20001,"登录失败");
        }

        //判断用户状态是否被占用
        if(userInfo.getIsDisabled().intValue() == 1){
            throw new MySwiftException(20001,"登录失败");
        }

        //登录成功生成token并返回
        String token = JwtHelper.createToken(userInfo.getId(), userInfo.getNickname());
        return token;

    }
}
