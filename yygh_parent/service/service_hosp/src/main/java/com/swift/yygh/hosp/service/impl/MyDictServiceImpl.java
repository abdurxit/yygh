package com.swift.yygh.hosp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swift.yygh.hosp.mapper.MyDictMapper;
import com.swift.yygh.hosp.service.MyDictService;
import com.swift.yygh.model.cmn.Dict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyDictServiceImpl extends ServiceImpl<MyDictMapper, Dict> implements MyDictService {

    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public List<Dict> getDictList(Long parentId) {
        String key = "dict:info:"+parentId;
        Object value = redisTemplate.opsForValue().get(key);
        if(value != null){
            return (List<Dict>) value;
        }
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",parentId);
        List<Dict> dictList = baseMapper.selectList(queryWrapper);
        dictList.forEach(dict -> {
            QueryWrapper<Dict> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("parent_id",dict.getId());
            Integer count = baseMapper.selectCount(queryWrapper1);
            boolean result = count > 0 ;
            dict.setHasChildren(result);
        });
        redisTemplate.opsForValue().set(key,dictList);
        return dictList;
    }
}
