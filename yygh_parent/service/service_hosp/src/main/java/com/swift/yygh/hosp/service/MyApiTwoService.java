package com.swift.yygh.hosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.hosp.vo.LoginVo;
import com.swift.yygh.model.hosp.HospitalSet;
import org.springframework.web.multipart.MultipartFile;

public interface MyApiTwoService extends IService<HospitalSet> {
    String upload(MultipartFile file);

    String uploadVideo(MultipartFile file);

    Boolean sendCode(String phone);

    String login(LoginVo loginVo);
}
