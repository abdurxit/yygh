package com.swift.yygh.hosp.controller.api;

import com.alibaba.fastjson.JSONObject;

import com.swift.yygh.common.result.Result;
import com.swift.yygh.hosp.service.ScheduleService;
import com.swift.yygh.model.hosp.Department;
import com.swift.yygh.model.hosp.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/hosp")
public class ApiScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping("/saveSchedule")
    public Result saveSchedule(@RequestParam Map<String,String> paramMap){
        //1.signKey校验
        Schedule schedule = JSONObject.parseObject(JSONObject.toJSONString(paramMap), Schedule.class);
        scheduleService.saveSchedule(schedule);
        return Result.ok();
    }


    @PostMapping("/schedule/list")
    public  Result<Page> page(@RequestParam Map<String,String> paramMap){
        //1.进行signKey校验
        Page<Schedule> page= scheduleService.page(paramMap);
        return Result.ok(page);
    }
}
