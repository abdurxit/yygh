package com.swift.yygh.hosp.controller.user;

import com.swift.yygh.common.result.R;
import com.swift.yygh.hosp.service.ScheduleService;
import com.swift.yygh.model.hosp.Schedule;
import com.swift.yygh.vo.hosp.ScheduleOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user/schedule")
public class UserScheduleController {


    @Autowired
    private ScheduleService scheduleService;

    @GetMapping("/page/{hoscode}/{depcode}/{pageNum}/{pageSize}")
    public R page(@PathVariable String hoscode,
                  @PathVariable String depcode,
                  @PathVariable Integer pageNum,
                  @PathVariable Integer pageSize){

        Map<String,Object> map= scheduleService.selectpage(hoscode,depcode,pageNum, pageSize);
        return R.ok().data(map);
    }

    @GetMapping("/detail/{hoscode}/{depcode}/{workDate}")
    public R detail(@PathVariable String hoscode,
                    @PathVariable String depcode,
                    @PathVariable String workDate){

        List<Schedule> scheduleList=scheduleService.getDetailSchedule(hoscode,depcode,workDate);
        return R.ok().data("list",scheduleList);
    }

    @GetMapping("/info/{id}")
    public R info(@PathVariable String id){
        Schedule schedule= scheduleService.info(id);
        return R.ok().data("schedule",schedule);
    }

    @GetMapping("/remote/{id}")
    public ScheduleOrderVo remote(@PathVariable("id") String id){
        ScheduleOrderVo schedule= scheduleService.remote(id);
        return schedule;
    }
}
