package com.swift.yygh.hosp.controller.api;


import com.swift.yygh.common.exception.MyYyghException;
import com.swift.yygh.common.result.Result;
import com.swift.yygh.common.util.HttpRequestHelper;
import com.swift.yygh.hosp.service.HospitalService;
import com.swift.yygh.hosp.service.HospitalSetService;
import com.swift.yygh.model.hosp.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api/hosp")
public class ApiHospitalController {


    @Autowired
    private HospitalSetService hospitalSetService;
    @Autowired
    private HospitalService hospitalService;


    @PostMapping("/saveHospital")
    public Result saveHospital(HttpServletRequest request){
        Map<String, String> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
        //1.进行signkey校验
        String requestSignKey = paramMap.get("sign");
        String platformSignKey= hospitalSetService.getPlatformSignKey(paramMap.get("hoscode"));

        if(StringUtils.isEmpty(requestSignKey) || StringUtils.isEmpty(platformSignKey) || !requestSignKey.equals(platformSignKey)){
             throw new MyYyghException(2001,"signkey有误!");
        }

        //2.保存医院信息到mongo中
        String logoData = paramMap.get("logoData");
        paramMap.put("logoData",logoData.replaceAll(" ", "+"));
        hospitalService.save(paramMap);

        return Result.ok();
    }


    @PostMapping("/hospital/show")
    public Result<Hospital> getHospitalInfo(@RequestParam Map<String,String> paramMap){
       //1.进行signkey校验:
        //2.
         String hoscode = paramMap.get("hoscode");
         Hospital hospital= hospitalService.getHospitalInfo(hoscode);
         return Result.ok(hospital);
    }
}
