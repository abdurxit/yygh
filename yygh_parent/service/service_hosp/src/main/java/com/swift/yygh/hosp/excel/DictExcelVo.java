package com.swift.yygh.hosp.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

@Data
public class DictExcelVo {
    @ExcelProperty(value = "分类ID",index = 0)
    @ColumnWidth(20)
    private Long id;
    @ExcelProperty(value = "父节点ID",index = 1)
    @ColumnWidth(20)
    private Long parentId;
    @ExcelProperty(value = "分类名称",index = 2)
    @ColumnWidth(20)
    private String name;


}
