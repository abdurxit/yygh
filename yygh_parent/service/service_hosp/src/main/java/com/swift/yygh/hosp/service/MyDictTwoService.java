package com.swift.yygh.hosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.cmn.Dict;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface MyDictTwoService extends IService<Dict> {
    List<Dict> getDictList(Long parentId);

    void downloadExcel(HttpServletResponse response) throws Exception;

    void uploadExcel(MultipartFile file) throws Exception;
}
