package com.swift.yygh.hosp.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.swift.yygh.hosp.excel.HospitalSetExcelVo;
import com.swift.yygh.hosp.mapper.MyApiMapper;
import com.swift.yygh.model.hosp.HospitalSet;
import org.springframework.beans.BeanUtils;

public class UploadHospListener extends AnalysisEventListener<HospitalSetExcelVo> {

    private MyApiMapper myApiMapper;

    public UploadHospListener(MyApiMapper myApiMapper) {
        this.myApiMapper = myApiMapper;
    }

    @Override
    public void invoke(HospitalSetExcelVo data, AnalysisContext context) {
        HospitalSet hospitalSet = new HospitalSet();
        BeanUtils.copyProperties(data,hospitalSet);
        HospitalSet hospitalSet1 = myApiMapper.selectById(hospitalSet.getId());
        if(hospitalSet1 == null){
            myApiMapper.insert(hospitalSet);
        }else {
            myApiMapper.updateById(hospitalSet);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
