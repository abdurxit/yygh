package com.swift.yygh.hosp.controller.my;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.swift.yygh.common.result.R;

import com.swift.yygh.common.util.MD5;
import com.swift.yygh.hosp.service.MyApiTwoService;
import com.swift.yygh.hosp.service.MyDictService;
import com.swift.yygh.hosp.service.MyDictTwoService;
import com.swift.yygh.hosp.vo.HospitalSetQueryVo;
import com.swift.yygh.hosp.vo.LoginVo;
import com.swift.yygh.model.cmn.Dict;
import com.swift.yygh.model.hosp.Hospital;
import com.swift.yygh.model.hosp.HospitalSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(tags = "myTwo接口")
@RestController
@RequestMapping("my")
public class MyApiTwoController {

    @Autowired
    private MyApiTwoService myApiTwoService;

    @Autowired
    private MyDictTwoService myDictTwoService;



    @ApiOperation("查询所有接口")
    @PostMapping("/findListTwo/{page}/{limit}")
    public R findList(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit,
                      @RequestBody HospitalSetQueryVo hospitalSetQueryVo){
//        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
//        queryWrapper.like(!StringUtils.isEmpty(hospitalSetQueryVo.getHosname()),"hosname",hospitalSetQueryVo.getHosname());
//        queryWrapper.eq(!StringUtils.isEmpty(hospitalSetQueryVo.getHoscode()),"hoscode",hospitalSetQueryVo.getHoscode());
//        Page<HospitalSet> hospitalSetPage = myApiTwoService.page(new Page<>(page,limit),queryWrapper);

        LambdaQueryWrapper<HospitalSet> wrapper = Wrappers.lambdaQuery(HospitalSet.class).eq(!StringUtils.isEmpty(hospitalSetQueryVo.getHoscode()),HospitalSet::getHoscode, hospitalSetQueryVo.getHoscode())
                .like(!StringUtils.isEmpty(hospitalSetQueryVo.getHosname()),HospitalSet::getHosname, hospitalSetQueryVo.getHosname());
        Page<HospitalSet> hospitalSetPage = myApiTwoService.page(new Page<>(page, limit), wrapper);
        return R.ok().data("total",hospitalSetPage.getTotal()).data("rows",hospitalSetPage.getRecords());
    }



    @ApiOperation("新增")
    @PostMapping("/insertTwo")
    public R save(@RequestBody HospitalSet hospitalSet){
        hospitalSet.setStatus(0);
        String signKey = hospitalSet.getSignKey();
        hospitalSet.setSignKey(MD5.encrypt(signKey));
        boolean save = myApiTwoService.save(hospitalSet);
        if(save){
            return R.ok().message("新增成功");
        }else {
            return R.error().message("新增失败");
        }
    }

    @ApiOperation("根据id查询回显")
    @GetMapping("selectByIdTwo/{id}")
    public R selectById(@PathVariable("id") Long id){
        HospitalSet hospitalSet = myApiTwoService.getById(id);
        if(hospitalSet !=null){
            hospitalSet.setSignKey(null);
        }
        return R.ok().data("item",hospitalSet);
    }

    @ApiOperation("修改")
    @PutMapping("updateTwo")
    public R update(@RequestBody HospitalSet hospitalSet){
        String signKey = hospitalSet.getSignKey();
        hospitalSet.setSignKey(MD5.encrypt(signKey));
        boolean update = myApiTwoService.saveOrUpdate(hospitalSet);
        if(update){
            return R.ok().message("修改成功");
        }else {
            return R.error().message("修改失败");
        }
    }

    @ApiOperation("修改状态")
    @PutMapping("updateStatusTwo/{id}/{status}")
    public R updateStatus(@PathVariable("id") Long id,@PathVariable("status") Integer status){
        HospitalSet hospitalSet = myApiTwoService.getById(id);
        hospitalSet.setStatus(status);
        boolean update = myApiTwoService.updateById(hospitalSet);
        if(update){
            return R.ok().message("修改状态成功");
        }else {
            return R.error().message("修改状态失败");
        }
    }


    @ApiOperation("根据Id删除接口")
    @DeleteMapping("removeByIdTwo/{id}")
    public R delete(@PathVariable("id") Long id){
        boolean remove = myApiTwoService.removeById(id);
        if(remove){
            return R.ok().message("删除成功");
        }else {
            return R.error().message("删除失败");
        }
    }


    @ApiOperation("批量删除接口")
    @DeleteMapping("batchRemoveTwo")
    public R batchRemove(@RequestBody List<Long> idList){
        boolean remove = myApiTwoService.removeByIds(idList);
        if(remove){
            return R.ok().message("批量删除成功");
        }else {
            return R.error().message("批量删除失败");
        }
    }

    @ApiOperation("图片上传接口")
    @PostMapping("/uploadTwo")
    public R upload(@RequestParam("file")MultipartFile file){
       String url =  myApiTwoService.upload(file);
       return R.ok().data("url",url).message("图片上传成功");
    }



    @ApiOperation("根据父节点id查询子节点数据")
    @GetMapping("getDictListByIdTwo/{parentId}")
    public R getDictList(@PathVariable("parentId") Long parentId){
        List<Dict> dictList = myDictTwoService.getDictList(parentId);
    return R.ok().data("list",dictList);
    }



    @ApiOperation("导出Excel接口")
    @GetMapping("downloadTwo")
    public void download(HttpServletResponse response) throws Exception{
        myDictTwoService.downloadExcel(response);
    }


    @ApiOperation("导入Excel接口")
    @PostMapping("uploadExcelTwo")
    public R uploadExcelTwo(@RequestParam("file") MultipartFile file) throws Exception{
        myDictTwoService.uploadExcel(file);
        return R.ok();
    }

    @ApiOperation("视频上传接口")
    @PostMapping("uploadVideoTwo")
    public R uploadVideoTwo(@RequestParam("file") MultipartFile file){
       String videoId =  myApiTwoService.uploadVideo(file);
       return R.ok().data("videoId",videoId);
    }


    @ApiOperation("发送短信验证码接口")
    @GetMapping("sendCode/{phone}")
    public R sendCodeTwo(@PathVariable("phone") String phone){
       Boolean result =  myApiTwoService.sendCode(phone);
       if(result){
           return R.ok().message("短信发送成功");
       }else {
           return R.error().message("短信发送失败");
       }
    }


    @ApiOperation("手机号短信验证码登录")
    @PostMapping("loginTwo")
    public R login(@RequestBody LoginVo loginVo){
       String token =  myApiTwoService.login(loginVo);
       return R.ok().data("token",token);
    }






































}
