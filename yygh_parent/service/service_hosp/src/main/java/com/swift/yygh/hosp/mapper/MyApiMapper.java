package com.swift.yygh.hosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swift.yygh.model.hosp.HospitalSet;

public interface MyApiMapper extends BaseMapper<HospitalSet> {
}
