package com.swift.yygh.hosp.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.swift.yygh.model.hosp.HospitalSet;

/**
 * <p>
 * 医院设置表 服务类
 * </p>
 *
 * @author swift
 * @since 2022-12-27
 */
public interface HospitalSetService extends IService<HospitalSet> {
    String getPlatformSignKey(String hoscode);
}
