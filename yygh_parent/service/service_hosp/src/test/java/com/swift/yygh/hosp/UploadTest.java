package com.swift.yygh.hosp;

import com.alibaba.excel.EasyExcel;
import com.swift.yygh.hosp.excel.HospitalSetExcelVo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UploadTest {

    @Test
    public void test(){
        String fileName = "F:\\002.xlsx";
        EasyExcel.read(fileName, HospitalSetExcelVo.class,new HosUploadListener()).sheet().doRead();
    }
}
