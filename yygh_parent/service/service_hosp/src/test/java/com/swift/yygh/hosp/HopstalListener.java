package com.swift.yygh.hosp;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.swift.yygh.hosp.excel.HospitalSetExcelVo;

import java.util.ArrayList;
import java.util.List;

public class HopstalListener extends AnalysisEventListener<HospitalSetExcelVo> {
    List<HospitalSetExcelVo> list = new ArrayList<>();

    @Override
    public void invoke(HospitalSetExcelVo data, AnalysisContext context) {
        list.add(data);
        System.out.println(list);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
