package com.swift.yygh.hosp;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.swift.yygh.hosp.excel.HospitalSetExcelVo;
import com.swift.yygh.model.hosp.HospitalSet;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class HosUploadListener extends AnalysisEventListener<HospitalSetExcelVo> {

    List<HospitalSet> hospitalSetList = new ArrayList<>();
    @Override
    public void invoke(HospitalSetExcelVo data, AnalysisContext context) {
          HospitalSet hospitalSet = new HospitalSet();
        BeanUtils.copyProperties(data,hospitalSet);
        hospitalSetList.add(hospitalSet);
        System.out.println(hospitalSetList);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
