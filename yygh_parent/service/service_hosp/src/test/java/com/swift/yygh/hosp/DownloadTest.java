package com.swift.yygh.hosp;

import com.alibaba.excel.EasyExcel;
import com.swift.yygh.hosp.excel.HospitalSetExcelVo;
import com.swift.yygh.hosp.service.MyApiService;
import com.swift.yygh.model.hosp.HospitalSet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class DownloadTest {

    @Autowired
    private MyApiService myApiService;
    @Test
    public void test01(){

        List<HospitalSet> hospitalSetList = myApiService.list();
        List<HospitalSetExcelVo> hospitalSetExcelVos = new ArrayList<>();
         hospitalSetList.forEach(hospitalSet -> {
            HospitalSetExcelVo hospitalSetExcelVo = new HospitalSetExcelVo();
             BeanUtils.copyProperties(hospitalSet,hospitalSetExcelVo);
             hospitalSetExcelVos.add(hospitalSetExcelVo);
        });

        String fileName = "F:\\002.xlsx";
        EasyExcel.write(fileName, HospitalSetExcelVo.class).sheet("医院设置数据").doWrite(hospitalSetExcelVos);
    }
}
