package com.swift.yygh.task.job;

import com.swift.yygh.mq.constant.MqConst;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class PatientRemindJob {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    //fixedRate:执行频率：
    //fixedDelay:当前任务执行完成之后延迟多长时间再执行下一个定时任务
    //cron:石英表达式
    @Scheduled(cron = "*/5 * * * * ?")
    public void remind() throws InterruptedException {
        System.out.println("定时任务执行了"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        //  Thread.sleep(3000);
        rabbitTemplate.convertAndSend(MqConst.EXCHANGE_TASK,MqConst.ROUTING_KEY_TASK,"");
    }
}
