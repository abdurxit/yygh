package com.swift.yygh.task.config;

import com.swift.yygh.mq.constant.MqConst;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean("exchange")
    public Exchange getExchange(){
      return   ExchangeBuilder.topicExchange(MqConst.EXCHANGE_TASK).build();
    }

    @Bean("queue")
    public Queue getQueue(){
        return QueueBuilder.durable(MqConst.QUEUE_TASK).build();
    }

    @Bean
    public Binding bindingQueueToExchange(@Qualifier("exchange") Exchange exchange,@Qualifier("queue") Queue queue){
       return    BindingBuilder.bind(queue).to(exchange).with("*.task.#").noargs();
    }


}
