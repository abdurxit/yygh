package com.swift.yygh.cmn.client;

import com.swift.yygh.cmn.client.fallback.DictFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-cmn",path = "/admin/cmn",fallbackFactory = DictFeignClientFallback.class)
public interface DictFeignClient {

    @GetMapping(value = "/getName/{value}/{dictCode}")
    public String getName(@PathVariable("value") Long value, @PathVariable("dictCode") String dictCode);

    @GetMapping(value = "/getName/{value}")
    public String getName(@PathVariable("value") Long value) ;
}
