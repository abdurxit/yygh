package com.swift.yygh.cmn.client.fallback;

import com.swift.yygh.cmn.client.DictFeignClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class DictFeignClientFallback implements FallbackFactory<DictFeignClient> {
    @Override
    public DictFeignClient create(Throwable cause) {
        return new DictFeignClient() {
            @Override
            public String getName(Long value, String dictCode) {
                return "地址获取失败！。。";
            }

            @Override
            public String getName(Long value) {
                return "等级获取失败！。。";
            }
        };
    }
}
