package com.swift.yygh.hosp.client;

import com.swift.yygh.vo.hosp.ScheduleOrderVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-hosp",path = "/user/schedule")
public interface ScheduleFeignClient {

    @GetMapping("/remote/{id}")
    public ScheduleOrderVo remote(@PathVariable("id") String id);
}
