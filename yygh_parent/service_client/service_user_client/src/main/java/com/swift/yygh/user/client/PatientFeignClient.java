package com.swift.yygh.user.client;

import com.swift.yygh.model.user.Patient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-user",path = "/user/patient")
public interface PatientFeignClient {

    @GetMapping("/remote/{id}")
    public Patient list(@PathVariable("id") Long id);
}
