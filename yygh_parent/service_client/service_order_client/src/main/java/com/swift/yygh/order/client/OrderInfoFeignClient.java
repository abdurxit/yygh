package com.swift.yygh.order.client;

import com.swift.yygh.vo.order.OrderCountQueryVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(value = "service-order",path = "/user/order")
public interface OrderInfoFeignClient {

    @PostMapping("/data")
    public Map<String,Object> getStatisticsData(@RequestBody OrderCountQueryVo orderCountQueryVo);

}
