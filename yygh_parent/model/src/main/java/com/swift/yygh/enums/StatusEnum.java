package com.swift.yygh.enums;

public enum StatusEnum {

    LOCK(0,"锁定"),
    NORMAL(1,"正常")
    ;
    private Integer code;
    private String desc;

    StatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getDescByCode(Integer code){
        StatusEnum[] values = StatusEnum.values();
        for (StatusEnum statusEnum : values) {
            if(code.intValue() ==  statusEnum.getCode().intValue()){
                return statusEnum.getDesc();
            }
        }
        return "";
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
