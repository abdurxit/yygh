package com.swift.yygh.common.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MySwiftException extends RuntimeException{
    private Integer code;
    private String msg;
}
