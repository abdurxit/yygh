package com.swift.yygh.common.exception;


import com.swift.yygh.common.result.R;
import lombok.extern.slf4j.Slf4j;
;
import org.apache.ibatis.reflection.ExceptionUtil;
import org.apache.tomcat.util.ExceptionUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@ControllerAdvice //表示全局异常处理类
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class) //针对所有微服务的Controller接口
   // @ResponseBody
    public R error(Exception e){
        e.printStackTrace();
        log.error(e.getMessage());
        return R.error();
    }

    @ExceptionHandler(value = RuntimeException.class)
    public R runTimeException(RuntimeException e){
        e.printStackTrace();
        return R.error().message("运行时异常");
    }

    @ExceptionHandler(value =ArithmeticException.class)
    public R arithmeticException(ArithmeticException e){
        e.printStackTrace();
        return R.error().message("算术运算异常");
    }

    //自定义异常处理
    @ExceptionHandler(value = MyYyghException.class)
    public R myException(MyYyghException e){
        e.printStackTrace();
        return R.error().code(e.getCode()).message(e.getMsg());
    }
    @ExceptionHandler(MySwiftException.class)
    public R exception(MySwiftException e){
        e.printStackTrace();
        return R.error().code(e.getCode()).message(e.getMsg());
    }
}
