package com.swift.yygh.common.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2//开启Swagger支持
public class SwaggerConfig {


    @Bean
    public Docket getAdminDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getAdminApiInfo())
                .groupName("管理员组")
                .select()
                .paths(Predicates.and(PathSelectors.regex("/admin/.*")))
                .build();
    }

    @Bean
    public Docket getMyPrimaryDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getMyApiInfo())
                .groupName("我的个人api组")
                .select()
                .paths(Predicates.and(PathSelectors.regex("/my/.*")))
                .build();
    }

    @Bean
    public ApiInfo getAdminApiInfo(){
       return new ApiInfoBuilder()
                .title("管理员系统-API文档")
                .description("管理员系统微服务接口定义")
                .version("1.0")
                .contact(new Contact("swift","www.swift.com","swift@123.com"))
                .build();
    }

    @Bean
    public ApiInfo getMyApiInfo(){
        return new ApiInfoBuilder()
                .title("个人编程-API文档")
                .description("个人编程系统微服务接口定义")
                .version("1.0")
                .contact(new Contact("abdurxit","www.swift.com","swift@123.com"))
                .build();
    }

    @Bean
    public Docket getUserDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getUserApiInfo())
                .groupName("用户组")
                .select()
                .paths(Predicates.and(PathSelectors.regex("/user/.*")))
                .build();
    }

    @Bean
    public ApiInfo getUserApiInfo(){
        return new ApiInfoBuilder()
                .title("用户系统-API文档")
                .description("用户系统微服务接口定义")
                .version("1.0")
                .contact(new Contact("swift","www.swift.com","swift@123.com"))
                .build();
    }

    @Bean
    public Docket getApiDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                .groupName("医院组")
                .select()
                .paths(Predicates.and(PathSelectors.regex("/api/.*")))
                .build();
    }

    @Bean
    public ApiInfo getApiInfo(){
        return new ApiInfoBuilder()
                .title("医院系统-API文档")
                .description("医院系统微服务接口定义")
                .version("1.0")
                .contact(new Contact("swift","www.swift.com","swift@123.com"))
                .build();
    }




}
