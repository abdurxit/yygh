package com.swift.yygh.mq.constant;

public class MqConst {

    public static final String EXCHANGE_ORDER="exchange_order";
    public static final String ROUTING_KEY_ORDER="routing_key_order";
    public static final String QUEUE_ORDER="queue_order";


    public static final String EXCHANGE_SMS="exchange_sms";
    public static final String ROUTING_KEY_SMS="routing_key_sms";
    public static final String QUEUE_SMS="queue_sms";

    public static final String EXCHANGE_TASK="exchange_task";
    public static final String ROUTING_KEY_TASK="routing_key.task.abc";
    public static final String QUEUE_TASK="queue_task";
}
