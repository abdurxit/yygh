package com.swift.yygh.common.util;

import java.util.*;

public class HttpRequestHelper {

    /**
     * 获取时间戳
     *
     * @return
     */

    public static Map<String, String> switchMap(Map<String, String[]> paramMap) {
        Map<String, String> resultMap = new HashMap<String, String>();

        Set<Map.Entry<String, String[]>> entries = paramMap.entrySet();
        for (Map.Entry<String, String[]> entry : entries) {
            String key = entry.getKey();
            String[] value = entry.getValue();
            resultMap.put(key, value[0]);
        }
        return resultMap;
    }

}
