# 在线医院挂号项目

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程
1.Mysql8  ： 在192.168.56.14 中执行 systemctl start mysqld.service 命令启动

2.nginx : 在192.168.56.14 的/usr/local/nginx/sbin/ 目录下执行 ./nginx 命令启动

3.nacos :在 192.168.56.12 的 /opt/nacos/nacos/bin 目录下执行 ./startup.sh 命令启动

4.redis ：在 192.168.56.12 的 /usr/local/bin 目录下执行 redis-server /myredis/redis.conf 命令启动

5.mongodb: 在 192.168.56.12 的 /usr/local/mongodb/bin 目录下执行 mongod -f mongodb.conf 命令启动

6.rabbitmq: 在 192.168.56.13 中 执行 systemctl start rabbitmq-server.service 命令启动



 使用说明

1.  后台管理系统：在vue-admin-template-master 中npm run dev 命令启动

2.  系统功能展示如下

 2.1登录页面：
![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E7%99%BB%E5%BD%95%E9%A1%B5%E9%9D%A2%E5%9B%BE.jpg)   

 2.2管理员系统首页
![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E9%A6%96%E9%A1%B5%E5%9B%BE.jpg)

 2.3管理员系统医院设置管理
 
 ![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E5%8C%BB%E9%99%A2%E8%AE%BE%E7%BD%AE%E7%AE%A1%E7%90%86%E9%A1%B5%E5%9B%BE.jpg) 

2.4 管理员系统数据字典图
 
![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E6%95%B0%E6%8D%AE%E5%AD%97%E5%85%B8%E5%9B%BE.jpg)

2.5 管理员系统医院管理页图

![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E5%8C%BB%E9%99%A2%E7%AE%A1%E7%90%86%E9%A1%B5%E5%9B%BE.jpg)

2.6 管理员系统医院详情页图

![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E5%8C%BB%E9%99%A2%E8%AF%A6%E6%83%85%E9%A1%B5%E5%9B%BE.jpg)

2.7 管理系统用户列表图

 ![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E7%94%A8%E6%88%B7%E5%88%97%E8%A1%A8%E9%A1%B5%E5%9B%BE.jpg)

2.8 管理系统用户详情页图

 ![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E7%94%A8%E6%88%B7%E8%AF%A6%E6%83%85%E9%A1%B5%E5%9B%BE.jpg)

2.9 管理员系统用户认证列表图

 ![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E7%94%A8%E6%88%B7%E8%AE%A4%E8%AF%81%E5%88%97%E8%A1%A8%E5%9B%BE.jpg)

2.10 管理员系统统计管理图1

 ![输入图片说明](%E7%AE%A1%E7%90%86%E5%91%98%E7%B3%BB%E7%BB%9F%E7%BB%9F%E8%AE%A1%E7%AE%A1%E7%90%86%E9%A1%B5%E5%9B%BE.jpg)

2.11 管理员系统统计管理图2

 ![输入图片说明](%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F%E7%BB%9F%E8%AE%A1%E7%AE%A1%E7%90%86%E5%9B%BE2.jpg)

2.12 管理员系统统计管理图3
 
 ![输入图片说明](%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F%E7%BB%9F%E8%AE%A1%E7%AE%A1%E7%90%86%E5%9B%BE3.jpg)



3.  用户系统： 在 yygh-site 中执行npm run dev 命令启动


4.用户系统功能展示如下

  4.1  用户系统首页图片

 ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E9%A6%96%E9%A1%B5%E5%9B%BE.jpg)

  
  4.2 用户系统短信登录图
 
 ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E7%9F%AD%E4%BF%A1%E7%99%BB%E5%BD%95%E5%9B%BE.jpg)
  
  4.3 用户系统微信登录图
 
  ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E5%BE%AE%E4%BF%A1%E7%99%BB%E5%BD%95%E9%A1%B5%E5%9B%BE.jpg)
  
  4.4 用户系统实名认证图

  ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E5%AE%9E%E5%90%8D%E8%AE%A4%E8%AF%81%E9%A1%B5%E5%9B%BE.jpg)
  
  
  4.5 用户系统医院详情页图

   ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E5%8C%BB%E9%99%A2%E8%AF%A6%E6%83%85%E9%A1%B5%E5%9B%BE.jpg)


  4.6 用户系统预约挂号图

    ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E9%A2%84%E7%BA%A6%E6%8C%82%E5%8F%B7%E9%A1%B5%E5%9B%BE.jpg)

  4.7 用户系统确认挂号页图1

   ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E7%A1%AE%E8%AE%A4%E6%8C%82%E5%8F%B7%E9%A1%B5%E5%9B%BE1.jpg)
  4.8  用户系统确认挂号页图2       

  ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E7%A1%AE%E8%AE%A4%E6%8C%82%E5%8F%B7%E5%9B%BE2.jpg)

 4.9 用户系统挂号订单页图1
  ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E6%8C%82%E5%8F%B7%E8%AE%A2%E5%8D%95%E9%A1%B5%E5%9B%BE1.jpg)

 4.10 用户系统挂号订单图2 

![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E6%8C%82%E5%8F%B7%E8%AE%A2%E5%8D%95%E9%A1%B5%E5%9B%BE2.jpg)
 
4.11 用户系统挂号订单列表图
 ![输入图片说明](%E7%94%A8%E6%88%B7%E7%B3%BB%E7%BB%9F%E6%8C%82%E5%8F%B7%E8%AE%A2%E5%8D%95%E5%88%97%E8%A1%A8%E9%A1%B5%E5%9B%BE.jpg)









  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
